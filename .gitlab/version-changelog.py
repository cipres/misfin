#!/usr/bin/env python

import sys
import keepachangelog

ver = sys.argv[1]

changes = keepachangelog.to_raw_dict("CHANGELOG.md")

if ver in changes:
    print(changes[ver]['raw'])
