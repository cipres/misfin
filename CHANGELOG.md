## [1.2.4] - 2024-07-26

### Added

- Endpoint to unload decryption key
- Links to decrypt messages and cache key for a certain duration
- send-file: add binary and file size checks

### Changed

- Encrypted messages can be deleted from the inbox
- The "send-as" command can handle multiple recipients
- The "send-file" command can handle multiple recipients

## [1.2.3] - 2024-06-29

### Added

- Support for gembox message encryption (via EC curve25519)
- Support for encrypted file uploads
- Use the new gemeaux routes mapper API

### Changed

- Use a time-adaptive sparing cache for gemboxes. Gemboxes which use
  encryption are spared from being purged by the cache if a privkey is active.
- Spare active encrypted gemboxes from destruction every time the inbox page
  is accessed. This means that the user doesn't have to input the private key
  every time the gembox TTL expires, but rather only when the user hasn't accessed
  the inbox for some time.

## [1.2.2] - 2024-06-25

### Added

- Support for "Out-of-Office" (absence) automatic replies.
- Frontend interface to declare an absence (start/end date and autoreply message)
- Esperanto translations for the frontend interface messages

## [1.2.1] - 2024-06-21

### Added

- Support i18n in the frontend using the babel jinja2 extension
- French translations for the frontend interface messages
- Castilian Spanish translations for the frontend interface messages

### Changed

- Add an "About misfin" section in the "guest" page
- Cache the loaded gettext translations in a TTLCache
- The mailbox sign up process can be done in any of the supported languages

## [1.2.0] - 2024-06-19

### Added

- Added a page to create, list and subscribe to misfin mailing lists
- Store the mailing list's creator/admin in the ML config

## [1.1.9] - 2024-05-11

### Added

- Basic support for message drafts in the frontend
- Virtual mailboxes (added an "echo service" example in the docs)

### Fixed

- MH/Maildir mailboxes: force utf-8 charset when storing messages

## [1.1.8] - 2024-04-15

### Added

- Handle titan requests
- Support for importing contacts in the address book via a Titan upload request

## [1.1.7] - 2024-04-07

### Added

- Add links in the frontend to delete contacts from the address book
- Support for mail signatures
- Make it possible to block certain misfin addresses from the frontend
- Make it possible to block certain cert fingerprints from the frontend
- Config setting to control the verbosity of message delivery logs
- User settings: the user can now enable/disable the confirmation
  prompt when deleting messages
- Support for gembox message encryption (via EC curve25519)

### Changed

- Use Jinja2 (instead of genshi) templates for the frontend pages
- Typing hints

## [1.1.6] - 2024-02-14

### Added

- Handle Misfin(C) requests
- AppRun: log the git commit SHA when starting the service
- tests/test_protocol.py: Unit tests for B & C incoming requests

### Fixed

- Fix Letter._extract_recipients()

## [1.1.5] - 2024-02-13

### Added

- Added a "version" command that shows the software version and the
  git SHA commit (when using the AppImage)
- CI: misfin release notifications (with misfin mails) for misfin server admins

### Changed

- CI: get rid of setup.py, use "python -m build" instead

## [1.1.4] - 2024-02-10

### Added

- gembox messages migration when switching backends in the settings
- signup requests throttling via a rate-limiting signup bucket
- Store the config defaults for misfind in misfind_default.toml, and
  merge the defaults with the user's config with mergedeep.

### Changed

- setup.py => pyproject.toml (PEP 621)

## [1.1.3] - 2024-02-10

### Added

- letter.migrate_gembox(): a function that migrate messages from one
  gembox to another
- tests/conftest.py

### Changed

- Disable the use of locks by default for MH and Maildir gemboxes
- Identity.longform(): don't show "No name" if there's no blurb
- inbox_home template: only show the message status if it's READ or NEW

## [1.1.2] - 2024-02-08

### Added

- Config setting to disable user signups
- Config settings for the templates header and footer

### Fixed

- Fix a timestamp bug inherited by the old misfin repo: the Letter's
  "received_at" datetime variable was set to a default value in the kwargs,
  forcing all gemmail messages to have the same received date !
- Don't store messages with empty payloads (verification messages)
- Frontend: show the newest messages first
- gemmail: format datetimes as UTC (with "Z" aka zero offset)

## [1.1.1] - 2024-02-05

### Added

- open_box() in the letter module

### Fixed

- Send a 59 error in receive_from() when we can't handle the request
- Fix a few things in the gemboxreader

## [1.1.0] - 2023-10-16

### Added

- SMTP bridge service. Emails sent to the SMTP bridge will be delivered to
  the gembox of the recipient(s) if they have a misfin identity on the server.

## [1.0.9] - 2023-10-09

### Added

- Admin interface in the frontend, by logging with the server certificate
- Support for mailing lists. By default, for subscriptions, a mailing list will
  require validation by sending a verification code to the subscriber address

### Changed

- Use SO_REUSEADDR on the server socket
- Set a default timeout on the server socket
- Handle requests in the server's threadpool

## [1.0.8] - 2023-10-08

### Added

- Configurable server socket rate limiting with pyrate-limiter. The bucket acquiring
  item key is the peer's certificate fingerprint, meaning that the rate-limiting
  is done on an individual basis. There are 2 different rate limiters, one for anonymous
  requests and one for requests made with a valid certificate.
- CLI Unit tests

## [1.0.7] - 2023-10-07

### Added

- Mailbox config settings (dedicated toml file per identity)
- Add a /fetch endpoint to serialize any mailbox to a gembox file
- Frontend: add pages for mailbox settings and address book
- Simple address book: list and add contacts
- Support for maildir and "read" messages in the mailbox UI
- Config endpoint to change the mailbox type

### Changed

- misfin.letter.Letter: some refactoring to support the maildir format

## [1.0.6] - 2023-10-05

### Added

- The frontend app is now served on the same port as the misfin server
- Make it possible to daemonize the service, and use the logging module

### Changed

- Require a hostname/domain for "server-init"

## [1.0.5] - 2023-09-30

### Added

- Letter: parse the subject in messages
- Add a --subject option for the send-as command

## [1.0.4] - 2023-09-29

### Added

- Frontend gemini service

## [1.0.3] - 2023-09-28

### Added

- Add --bind and --ipv6 options for the "receive-as" command
- Add --ipv6 option for the "send-as" command
- gembox as MH mailboxes

### Changed

- receive_forever(): autodetect ipv6 addresses

## [1.0.2] - 2023-09-27

### Added

- gemboxr: console gembox reader

## [1.0.1] - 2023-09-27

### Added

- Fix misfin.letter.Letter
- Add support for the gembox mailbox format
- Add unit tests for gemmail
- Add a server implementation to serve multiple mailboxes

## [1.0.0] - 2023-09-25

### Changed
- Cleanup the code and make it pass flake8
- Add a "misfin" command-line program
- Add setup.py
