from pathlib import Path
from cachetools import TTLCache, cached
from typing import List, Optional, Union

from datetime import datetime
from datetime import timedelta

import concurrent.futures
import mailbox
import re
import secrets
import shutil
import traceback
import toml
import tomlkit
from tomlkit import nl, table

from pyrate_limiter import Duration, Rate, Limiter

from . import logger
from . import default_mailbox_config_path
from . import default_mailbox_peer_config_path

from .cache import SparingCache

from .crypto import generate_keypair
from .crypto import load_pubkey_from_path
from .crypto import PublicKey

from .identity import LocalIdentity
from .identity import PeerIdentity
from .identity import Identity
from .misfin import Request, Response
from .misfin import send_as
from .misfin import max_content_length
from .letter import Letter
from .letter import MHGemBox
from .letter import MaildirGemBox
from .letter import GemBoxSingleFile
from .smtpb import SMTPBridgeServer


def parse_limits(limits: List[str]) -> List[Rate]:
    """
    Parses a list of rate limits and returns a list of Rate
    Each rate limit must be formatted as limit/period, e.g
    5/1m for "5 per minute", or 20/1h for "20 per hour"
    """
    rates_l = []

    for limit_desc in limits:
        try:
            ma = re.search(r'^(\d+)/(\d+)(s|m|h|d|w)?$', limit_desc)
            assert ma

            limit, dur, tper = int(ma.group(1)), int(ma.group(2)), ma.group(3)

            if tper == 's' or tper is None:
                interval = Duration.SECOND * dur
            elif tper == 'm':
                interval = Duration.MINUTE * dur
            elif tper == 'h':
                interval = Duration.HOUR * dur
            elif tper == 'd':
                interval = Duration.DAY * dur
            elif tper == 'w':
                interval = Duration.WEEK * dur
            else:
                continue

            rates_l.append(Rate(limit, interval))
        except BaseException:
            continue

    return rates_l


class ServerContext:
    def __init__(self,
                 root_path: Path,
                 server_cert_path: Path,
                 config: Optional[dict] = None,
                 daemonized: bool = False,
                 max_workers: int = 64):
        self.root: Path = root_path
        self.server_cert_path: Path = server_cert_path
        self.config = config
        self.daemonized: bool = daemonized
        self.frontend = None
        self.threadpool = concurrent.futures.ThreadPoolExecutor(
            max_workers=max_workers)

        self.__icache: TTLCache = TTLCache(16, 60)
        self.__gbcache = SparingCache(
            self.config.get('gembox_cache_size', 1024),
            default_ttl=self.config.get('gembox_cache_ttl', 60 * 5)
        )

        # SMTP bridge service
        self.smtpd: Optional[SMTPBridgeServer] = None

        # Rate limiters
        self.signup_limiter_global: Optional[Limiter] = None
        self.signup_limiter_user: Optional[Limiter] = None

    @property
    def identities_path(self):
        return self.root.joinpath('identities')

    @property
    def file_uploads_path(self):
        return self.root.joinpath('uploads')

    def upload_path_for_identity(self, identity: LocalIdentity):
        p = self.file_uploads_path.joinpath(identity.fingerprint)
        p.mkdir(parents=True, exist_ok=True)
        return p

    def upload_path_for_id(self, uploader_fp: str,
                           upload_id):
        return self.file_uploads_path.joinpath(
            uploader_fp).joinpath(upload_id)

    def get_upload_manifest(self, upload_id: str) -> tomlkit.document:
        manifest_path = self.file_uploads_path.joinpath(f'{upload_id}.toml')
        if not manifest_path.is_file():
            return None

        with open(manifest_path, 'rt') as fd:
            return tomlkit.load(fd)

    def write_upload_manifest(self, uploader: LocalIdentity,
                              upload_id: str,
                              filename: str,
                              filesize: int,
                              mimetype: str) -> None:
        manifest_path = self.file_uploads_path.joinpath(f'{upload_id}.toml')
        if manifest_path.is_file():
            raise ValueError(f'Upload with id {upload_id} already exists')

        doc = tomlkit.document()

        up = tomlkit.table()
        up.add('fingerprint', uploader.fingerprint)
        doc.add('uploader', up)

        doc.add(nl())
        doc.add('upload_id', upload_id)
        doc.add('filename', filename)
        doc.add('filesize', filesize)
        doc.add('mimetype', mimetype)

        with open(manifest_path, 'wt') as fd:
            tomlkit.dump(doc, fd)

    def setup_rate_limiters(self) -> None:
        """
        Setup the rate limiters used by the service.
        """

        rates = parse_limits(
            self.config['frontend']['signup']['global_ratelimits']
        )
        user_rates = parse_limits(
            self.config['frontend']['signup']['user_ratelimits']
        )

        self.signup_limiter_global = Limiter(rates) if rates else None
        self.signup_limiter_user = Limiter(user_rates) if user_rates else None

    @cached(TTLCache(16, 600))
    def virtual_ident(self, mailbox: str, blurb: str) -> LocalIdentity:
        """
        Builds a virtual identity as a child of the server's identity
        and cache it in a TTLCache.
        """
        return LocalIdentity.child_of(
            self.server_identity(),
            mailbox,
            blurb
        )

    def server_identity(self) -> LocalIdentity:
        return LocalIdentity.load_from_file(self.server_cert_path)

    def spare_gembox(self, fingerprint: str, ttl: int = 0) -> None:
        """
        Prevent a gembox from being recycled by the cache.
        """

        self.__gbcache.spare(fingerprint, ttl)

    def gembox_for_fingerprint(self, fingerprint: str):
        """
        Return the gembox for the given identity fingerprint.
        Gemboxes are cached with a TTL cache.
        """

        cached = self.__gbcache.get(fingerprint)
        if cached:
            return cached

        ident = self.ident_lookup(fingerprint)
        if ident:
            self.__gbcache.store(fingerprint,
                                 self.gembox_for(ident))
            return self.__gbcache.get(fingerprint)

    def gembox_for(self, identity: LocalIdentity):
        user_cfg = self.mailbox_config(identity)
        storage_cfg = user_cfg.get('storage', {'type': 'mh'})
        store_path = self.root.joinpath('gemboxes')

        if storage_cfg['type'] == 'mh':
            return MHGemBox(store_path.joinpath(
                f'{identity.fingerprint}.gembox'
            ))
        elif storage_cfg['type'] == 'maildir':
            return MaildirGemBox(store_path.joinpath(
                f'{identity.fingerprint}.maildir'
            ))
        elif storage_cfg['type'] == 'gembox-file':
            return GemBoxSingleFile(store_path.joinpath(
                f'{identity.fingerprint}.gemboxf'
            ))
        else:
            raise ValueError('Unsupported mailbox type')

    def idents_iter(self):
        certs = list(self.identities_path.glob('*.pem')) + \
            [self.server_cert_path]

        for cert_file in certs:
            certfp = str(cert_file)

            if certfp in self.__icache:
                yield self.__icache[certfp]
                continue

            try:
                identity = LocalIdentity.load_from_file(cert_file)
                assert identity._cert

                self.__icache[certfp] = identity

                yield identity
            except Exception:
                continue

    def ml_iter(self):
        for cfgp in self.identities_path.glob('*.toml'):
            try:
                with open(cfgp, 'rt') as cfgf:
                    cfg = toml.load(cfgf)

                identity = LocalIdentity.load_from_file(
                    cfgp.with_suffix('.pem')
                )

                assert cfg
                assert cfg.get('entity_type') == 'mailinglist'
                assert 'mailinglist' in cfg
                mlc = cfg['mailinglist']
                assert 'address' in mlc
                assert 'blurb' in mlc
            except AssertionError:
                continue
            except Exception:
                traceback.print_exc()
                continue
            else:
                yield mlc, mlc.get('creator'), identity

    def ident_lookup(self, fingerprint: str) -> Optional[LocalIdentity]:
        """
        Find an identity by matching its fingerprint
        """
        for ident in self.idents_iter():
            if ident.fingerprint == fingerprint:
                return ident

        return None

    def ident_for_address(self, address: str) -> Optional[LocalIdentity]:
        for identity in self.idents_iter():
            if identity.address() == address:
                return identity

        return None

    def mailbox_cert_path(self, mailbox: str) -> Path:
        """
        Returns the Path of the identity for a given mailbox
        """
        return self.identities_path.joinpath(f'{mailbox}.pem')

    def mailbox_config_path(self, identity: LocalIdentity) -> Path:
        """
        Returns the Path of the configuration for a mailbox
        """
        return self.identities_path.joinpath(f'{identity.mailbox}.toml')

    def mailbox_confd_path(self, identity: LocalIdentity) -> Path:
        """
        Returns the Path of the conf.d directory for a mailbox
        """
        return self.identities_path.joinpath(f'{identity.mailbox}.conf.d')

    def peer_config_path(self, identity: LocalIdentity,
                         peer_ident: Identity) -> Path:
        """
        Returns the Path of the config file for a given peer in relation
        to a mailbox.
        """
        return self.mailbox_confd_path(identity).joinpath(
            f'{peer_ident.fingerprint}.toml'
        )

    def mailbox_enckeys_pubkeyp(self, identity: LocalIdentity) -> Path:
        """
        Returns the Path to the mailbox's public key for encrypting messages
        """
        return self.identities_path.joinpath(
            f'{identity.mailbox}_mailenc.pub')

    def mailbox_enckeys_pubkey(self, identity: LocalIdentity) -> PublicKey:
        """
        Returns the mailbox's public key for encryption for this identity
        """
        return load_pubkey_from_path(self.mailbox_enckeys_pubkeyp(identity))

    def mailbox_has_enckeys(self, identity: LocalIdentity) -> bool:
        return self.mailbox_enckeys_pubkey(identity) is not None

    def mailbox_config(self,
                       identity: LocalIdentity,
                       mode: str = 'toml') -> Union[dict,
                                                    tomlkit.TOMLDocument]:
        """
        Returns the configuration for a mailbox
        """
        fp = self.mailbox_config_path(identity)
        if not fp.is_file():
            shutil.copy(default_mailbox_config_path(), str(fp))

        with open(fp, 'rt') as cfgf:
            if mode == 'toml':
                return toml.load(cfgf)
            else:
                return tomlkit.load(cfgf)

    def peer_config(self,
                    identity: LocalIdentity,
                    peer_ident: Identity,
                    mode: str = 'toml') -> Union[dict,
                                                 tomlkit.TOMLDocument]:
        """
        Returns the configuration for a peer
        """
        self.mailbox_confd_path(identity).mkdir(parents=True, exist_ok=True)

        fp = self.peer_config_path(identity, peer_ident)
        if not fp.is_file():
            shutil.copy(default_mailbox_peer_config_path(), str(fp))

        with open(fp, 'rt') as cfgf:
            if mode == 'toml':
                return toml.load(cfgf)
            else:
                return tomlkit.load(cfgf)

    def mailbox_config_write(self, identity: LocalIdentity, cfg: dict,
                             mode: str = 'toml') -> bool:
        """
        Writes the configuration for a mailbox
        """
        with open(self.mailbox_config_path(identity), 'wt') as fd:
            if mode == 'toml':
                toml.dump(cfg, fd)
            else:
                tomlkit.dump(cfg, fd)

        return True

    def mailbox_exists(self, mailbox: str) -> bool:
        try:
            if not self.mailbox_cert_path(mailbox).is_file():
                return False

            with open(self.mailbox_cert_path(mailbox), 'rb') as fd:
                pem = fd.read()

            identity = LocalIdentity(pem, pem)
            assert identity._cert

            return True
        except Exception:
            return False

    def create_mailbox(self,
                       parent: LocalIdentity,
                       mailbox: str,
                       blurb: str,
                       mtype: str = 'regular',
                       creator: Optional[LocalIdentity] = None) -> tuple:
        """
        Create a new mailbox
        """

        from tomlkit import nl, table
        try:
            assert not self.mailbox_exists(mailbox)

            ident = LocalIdentity.child_of(parent, mailbox, blurb)
            output = self.mailbox_cert_path(mailbox)

            pem = ident.as_pem()

            with open(output, "wb") as dest:
                dest.write(pem)

            if mtype == 'mailinglist':
                cfge = self.mailbox_config(ident, 'edit')

                entry = table()
                entry.add('address', ident.address())
                entry.add('blurb', blurb)
                entry.add('max_subscribers', 0)
                entry.add('date_created', datetime.utcnow().isoformat())

                # Requires validation: when enabled, the user will need to
                # enter a secret sent to his misfin address to confirm the sub
                entry.add('sub_requires_validation', True)

                if creator is not None:
                    centry = table()
                    centry.add('address', creator.address())
                    centry.add('blurb', creator.blurb)
                    centry.add('fingerprint', creator.fingerprint)
                    entry.add('creator', centry)

                cfge.add(nl()).add("mailinglist", entry).add(nl())
                cfge.add(nl()).add("entity_type", "mailinglist").add(nl())

                self.mailbox_config_write(ident, cfge, 'tomlkit')

            # Create the mailbox encryption keys
            # We only store the public key, the private key is only
            # shown once to the user and never stored

            pubkeyp, privk = self.create_enckeys_for_identity(ident)

            return ident, pem, privk
        except Exception:
            traceback.print_exc()
            return None, None, None

    def create_enckeys_for_identity(self, ident: LocalIdentity,
                                    force: bool = False) -> tuple:
        pubkeyp = self.mailbox_enckeys_pubkeyp(ident)

        pubkey = load_pubkey_from_path(pubkeyp)
        if not pubkey or force:
            privk, pubk = generate_keypair()
            pubkeyp.write_bytes(bytes(pubk))
            return pubkeyp, privk

        return None, None


def gembox_message_store(server_ctx: ServerContext,
                         storage_cfg: dict,
                         mailbox_cfg,
                         mailbox_identity,
                         peer: PeerIdentity,
                         request: Request,
                         letter: Letter) -> bool:
    """
    Store the message in a gembox
    """

    msgd_loglevel: int = server_ctx.config.get('msg_delivery_loglevel', 0)
    try:
        gembox = server_ctx.gembox_for_fingerprint(
            mailbox_identity.fingerprint)

        if not gembox:
            raise ValueError('No gembox for {mailbox_identity.fingerprint}')

        if msgd_loglevel > 1:
            logger.info(
                f"==> Delivering to gembox: {gembox.path} ({gembox.mbtype})"
            )

        cfg = server_ctx.mailbox_config(mailbox_identity)
        encrypt = cfg['storage'].get('encrypt_incoming_messages', False)

        enc_pubkeyp = server_ctx.mailbox_enckeys_pubkeyp(mailbox_identity)
        if encrypt and enc_pubkeyp.is_file():
            try:
                # Set the public key for the mailbox encryption
                gembox.set_encryption_pubkey(
                    load_pubkey_from_path(enc_pubkeyp)
                )
            except Exception:
                logger.warning(
                    f'Could not set encryption key for '
                    f'identity: {mailbox_identity.address()}'
                )
        else:
            gembox.set_encryption_pubkey(None)

        logger.info(
            f"==> Delivering to gembox: {gembox.path} ({gembox.mbtype})")

        gembox += letter

        return True
    except Exception:
        traceback.print_exc()
        return False


def mbox_message_store(server_ctx: ServerContext,
                       storage_cfg: dict,
                       mailbox_identity,
                       peer: PeerIdentity,
                       request: Request) -> bool:
    """
    Store the message in an mbox file
    """
    try:
        store_path = server_ctx.root.joinpath(
            storage_cfg.get('subpath', 'mboxes')
        )

        if not store_path.is_dir():
            store_path.mkdir(parents=True)

        # Path for the recipient's mbox
        mboxp = store_path.joinpath(mailbox_identity.fingerprint)

        logger.info(f"==> Delivering to mbox: {mboxp}")

        mbox = mailbox.mbox(str(mboxp))
        mbox.lock()
    except Exception:
        # ^_^
        traceback.print_exc()
        return False

    # Store the message in the mbox
    msg = mailbox.mboxMessage()
    msg['From'] = peer.address()
    msg['To'] = request.recipient.address()
    msg.set_payload(request.payload)
    mbox.add(msg)
    mbox.flush()
    mbox.unlock()

    return True


def ml_subscribers(server_ctx: ServerContext,
                   ml_identity: LocalIdentity):
    """
    Yields all subscribers of this mailing list.
    """
    cfge = server_ctx.mailbox_config(ml_identity, 'edit')

    if 'subscriber' in cfge:
        for key, sub in cfge['subscriber'].items():
            yield key, sub


def ml_handle_action(server_ctx: ServerContext,
                     storage_cfg: dict,
                     mailbox_identity,
                     peer: PeerIdentity,
                     request: Request,
                     letter: Letter) -> Response:

    sub_key, sub_entry = None, None
    cfge = server_ctx.mailbox_config(mailbox_identity, 'edit')
    aparams = letter.subject.lower().split(' ') if letter.subject else None
    action = aparams[0] if aparams else None

    # Handle subscribe and unsubscribe for existing subscribers
    for key, sub in ml_subscribers(server_ctx, mailbox_identity):
        if sub.get('fingerprint') == peer.fingerprint:
            sub_key = key
            sub_entry = sub
            if action == 'subscribe':
                return Response.of(
                    50, 'Already subscribed to this mailing list')
            elif action == 'unsubscribe':
                cfge['subscriber'].pop(key)

                server_ctx.mailbox_config_write(
                    mailbox_identity, cfge, 'tomlkit')
                return Response.of(20, 'Unsubscribed')

    requires_validation = cfge['mailinglist'].get('sub_requires_validation',
                                                  False)

    if action == 'subscribe':
        entry = table()

        if requires_validation:
            secret = secrets.token_hex(8)
            v_msg = Request(
                Identity(peer.address()),
                f'Reply with "# verify {secret}" to validate your subscription'
            )
            entry.add('validation_code', secret)
            entry.add('validation_code_expiry_date',
                      (datetime.utcnow() + timedelta(days=7)).isoformat())

            server_ctx.threadpool.submit(
                send_as,
                mailbox_identity,
                v_msg
            )

        entry.add('address', peer.address())
        entry.add('fingerprint', peer.fingerprint)

        if peer.blurb:
            entry.add('blurb', peer.blurb)

        entry.add('verified', False if requires_validation else True)

        entry.add('date_subscribed', datetime.utcnow().isoformat())

        sentry = table()
        sentry.add(peer.fingerprint, entry)
        cfge.add(nl()).add("subscriber", sentry).add(nl())

        server_ctx.mailbox_config_write(mailbox_identity, cfge, 'tomlkit')

        return Response.delivered(mailbox_identity.fingerprint)
    elif action == 'verify' and len(aparams) > 1 and sub_key:
        # Verify the code sent to the user to validate the subscription
        code = sub_entry.get('validation_code')

        if code == aparams[1]:
            cfge['subscriber'][sub_key]['verified'] = True

            server_ctx.mailbox_config_write(mailbox_identity, cfge, 'tomlkit')

            server_ctx.threadpool.submit(
                send_as,
                mailbox_identity,
                Request(Identity(peer.address()),
                        'Your subscription to the mailing list is complete')
            )
            return Response.delivered(mailbox_identity.fingerprint)
        else:
            return Response.of(40, "Invalid validation code!")
    else:
        return Response.of(40, f"Action {action} was not performed")


def serve_multi(server_ctx: ServerContext,
                server: LocalIdentity,
                peer: PeerIdentity,
                request: Request):
    """
    Message handler for a multi-mailbox server
    """

    now = datetime.now()
    msgd_loglevel: int = server_ctx.config.get('msg_delivery_loglevel', 0)
    mailbox_identity = None

    # Virtual mailboxes. Only handle autoreply for now.
    vmbs = server_ctx.config.get('vmailbox')

    if isinstance(vmbs, list):
        for vmb in vmbs:
            if not isinstance(vmb, dict):
                continue

            vmboxes = vmb.get('mailbox', [])
            autoreply = vmb.get('auto_reply')

            if not vmboxes or not autoreply:
                continue

            if isinstance(vmboxes, list):
                rcp_mb = [val for val in vmboxes if isinstance(val, str)]
            elif isinstance(vmboxes, str):
                rcp_mb = [vmboxes]
            else:
                continue

            if request.recipient.mailbox in rcp_mb:
                # Build the virtual identity (it will be cached)

                ident = server_ctx.virtual_ident(
                    request.recipient.mailbox,
                    vmb.get('blurb', request.recipient.mailbox)
                )

                if ident and isinstance(autoreply, str):
                    body = autoreply.format(payload=request.payload)

                    if len(body) >= max_content_length:
                        continue

                    server_ctx.threadpool.submit(
                        send_as,
                        ident,
                        Request(
                            Identity(peer.address()),
                            body
                        )
                    )

                    # Reply 20 without fingerprint
                    return Response.of(20)

    # Find the certificate that matches the recipient address
    for identity in server_ctx.idents_iter():
        try:
            assert server.verify(identity) is True

            if identity.address() == request.recipient.address():
                if msgd_loglevel > 1:
                    logger.info(f'{identity.address()} OK: '
                                f'{identity.fingerprint} is valid')

                mailbox_identity = identity
                break
        except AssertionError:
            continue
        except Exception:
            traceback.print_exc()
            continue

    if not mailbox_identity:
        if request.recipient.mailbox == server.mailbox:
            # For the server identity
            mailbox_identity = server
        else:
            # Mailbox not found, reply with 51
            return Response.of(51, 'Mailbox does not exist')

    if msgd_loglevel == 0:
        logger.info(
            f"Delivery: {mailbox_identity.fingerprint}"
        )
    elif msgd_loglevel == 1:
        logger.info(
            f"Delivery: {peer.fingerprint} => {mailbox_identity.fingerprint}"
        )
    elif msgd_loglevel >= 2:
        logger.info(
            f"Delivery: {peer.fingerprint} ({peer.address()}) "
            f"=> {mailbox_identity.fingerprint} "
            f"({request.recipient.address()})"
        )

    if len(request.payload) == 0:
        """
        Empty payload (messages used for verification)
        Don't store the message, and reply 20 as usual with the fingerprint
        """
        return Response.delivered(mailbox_identity.fingerprint)

    mailbox_cfg = server_ctx.mailbox_config(mailbox_identity)
    storage_cfg = mailbox_cfg.get('storage')

    block_addrs = mailbox_cfg.get('block_senders_byaddr', [])
    block_fingerprints = mailbox_cfg.get('block_senders_byfingerprint', [])

    abval = mailbox_cfg.get('absence')
    absences = abval if isinstance(abval, dict) else {}

    if isinstance(block_addrs, list) and peer.address() in block_addrs:
        # Blocking this address: return 61
        return Response.of(61)

    if isinstance(block_fingerprints, list) and \
       peer.fingerprint in block_fingerprints:
        # Blocking this fingerprint: return 61 as well
        return Response.of(61)

    try:
        assert storage_cfg
        store_type = storage_cfg.get('type', 'mh')
        store_result = None

        letter = Letter.incoming(peer, request)

        # Out-of-office (absence) autoreplies
        for abname, absence in absences.items():
            try:
                start_date = absence.get('start_date')
                end_date = absence.get('end_date')

                assert isinstance(start_date, datetime)
                assert isinstance(end_date, datetime)

                ab_autoreply = absence.get(
                    'autoreply_message',
                    'I am unavailable until {end}'
                ).format(
                    start=start_date.strftime('%Y-%m-%d'),
                    end=end_date.strftime('%Y-%m-%d')
                )

                ab_subject = absence.get('autoreply_subject')

                assert isinstance(ab_autoreply, str)

                if isinstance(ab_subject, str):
                    ab_autoreply = f'# {ab_subject}\n' + ab_autoreply

                assert len(ab_autoreply) < max_content_length
            except AssertionError:
                continue

            if letter.sender.address() == mailbox_identity.address():
                continue

            if now >= start_date and now <= end_date:
                server_ctx.threadpool.submit(
                    send_as,
                    mailbox_identity,
                    Request(letter.sender, ab_autoreply)
                )
                break

        # ML
        is_ml = mailbox_cfg.get('entity_type') == 'mailinglist' or \
            'mailinglist' in mailbox_cfg

        if is_ml:
            # This is a mailing list
            if letter.subject and letter.subject.split(' ')[0].lower() in \
                    ['subscribe', 'unsubscribe', 'verify']:
                # Mailing list action
                return ml_handle_action(
                    server_ctx,
                    storage_cfg,
                    mailbox_identity,
                    peer,
                    request,
                    letter
                )
            else:
                # Not an action: Forward the message to all subscribers.
                logger.info(
                    f"Message for mailing list: {request.recipient.address()}")

                for key, sub in ml_subscribers(server_ctx, mailbox_identity):
                    try:
                        recipient = sub['address']
                        verified = sub.get('verified', False)

                        if not verified:
                            # Subscriber hasn't been verified
                            continue

                        logger.debug(f"Relaying ML msg to: {recipient}")

                        payload = 'Message sent to this mailing list by: ' + \
                            f'{peer.address()}\n'

                        server_ctx.threadpool.submit(
                            send_as,
                            mailbox_identity,
                            Request(Identity(recipient),
                                    payload + request.payload)
                        )
                    except Exception:
                        logger.warning(
                            f'Failed to forward msg to: {recipient}'
                            f'{traceback.format_exc()}'
                        )
                        continue

        if store_type == 'mbox':
            store_result = mbox_message_store(
                server_ctx,
                storage_cfg,
                mailbox_identity,
                peer,
                request
            )
        elif store_type in ['mh', 'maildir', 'gembox-file']:
            # Add a timestamp
            letter.append_timestamp()

            store_result = gembox_message_store(
                server_ctx,
                storage_cfg,
                mailbox_cfg,
                mailbox_identity,
                peer,
                request,
                letter
            )
        else:
            raise ValueError(f'Unsupported mailbox type: {store_type}')

        assert store_result is True
        return Response.delivered(mailbox_identity.fingerprint)
    except Exception:
        traceback.print_exc()
        return Response.of(50, 'Cannot allocate mailbox')


def serve_single(server_ctx: ServerContext,
                 server: LocalIdentity, peer, request: Request):
    """
    Serve a single mailbox
    """

    logger.info(
        f"Incoming to {request.recipient.mailbox} from {peer.longform()}")
    logger.info("Fingerprint is {}".format(peer.fingerprint))
    logger.info("Message:")
    logger.info("{}".format(request.payload))

    if len(request.payload) == 0:
        return Response.delivered(server.fingerprint)

    if request.recipient.mailbox == server.mailbox or \
       request.recipient.mailbox in []:
        boxp = server_ctx.root.joinpath(
            f'{server.fingerprint}.gembox'
        )

        logger.info(f"==> Delivering to gembox: {boxp}")

        gembox = MHGemBox(boxp)
        gembox += Letter(
            Identity(peer.address()),  # type: ignore
            [
                Identity(request.recipient.address())
            ],
            request.payload
        )

        return Response.delivered(server.fingerprint)
    else:
        print(f"We aren't {request.recipient.mailbox}, we're {server.mailbox}")
        return Response.of(51)
