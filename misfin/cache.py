from datetime import datetime, timedelta

from cachetools import TLRUCache


def cache_ttu(_key, value, now):
    return now + timedelta(seconds=value._ttl_secs)


class SparingCache(TLRUCache):
    """
    A TLRU cache that can spare an item from destruction on demand
    """
    def __init__(self, maxsize: int, default_ttl: int = 60):
        self._default_ttl = default_ttl

        super().__init__(maxsize, ttu=cache_ttu, timer=datetime.now)

    def store(self, key: str, item, ttl: int = 0) -> None:
        item._ttl_secs = ttl if ttl > 0 else self._default_ttl
        self[key] = item

    def spare(self, key: str, ttl: int = 0) -> None:
        """
        Spare an item in the cache from being thrown out and recache it
        with the same TTL or a different TTL.
        """
        item = self.get(key)

        if item is not None:
            if ttl > 0:
                item._ttl_secs = ttl

            self[key] = item
