import socket
import datetime
import logging
import re
import traceback
import ipaddress
import secrets
from typing import Optional, Union

from concurrent.futures import CancelledError
from urllib.parse import parse_qs
from misfin.identity import Identity, PeerIdentity, LocalIdentity
from yarl import URL

from pyrate_limiter import Duration, Rate, Limiter
from pyrate_limiter import BucketFullException

import OpenSSL.SSL as ossl
import OpenSSL.crypto as ocrypt

# misfin - development misfin client/server
# misfin protocol requests, responses, and networking
# lem (2023)

# this is software in flux - don't rely on it


logger = logging.getLogger(__name__)


max_request_size = 2048
max_content_length = 16384

default_port = 1958


class InvalidRequestError(Exception):
    """
    Invalid request format exception
    """
    pass


class Request:
    """ A Misfin request - here's some data, here's where it's going. """

    TYPE_MISFIN = 0
    TYPE_GEMINI = 1
    TYPE_TITAN = 2

    MISFIN_PROTO_INVALID = -1
    MISFIN_PROTO_B = 0
    MISFIN_PROTO_C = 1

    def __init__(self, recipient: Identity, payload: str,
                 content_length: int = 0,
                 proto_version: int = MISFIN_PROTO_C,
                 req_type=TYPE_MISFIN, req_url=None,
                 titan_mime: Optional[str] = None,
                 titan_size: Optional[int] = None,
                 titan_data: Optional[Union[bytes, str]] = None):
        self.recipient = recipient
        self.payload = payload
        self.req_type = req_type
        self.req_url = req_url
        self.content_length = content_length
        self.proto_ver = proto_version

        self.titan_mime = titan_mime
        self.titan_size = titan_size
        self.titan_data = titan_data

        if proto_version == Request.MISFIN_PROTO_B and \
           len(self.build()) > max_request_size:
            raise ValueError(
                "Request is too large to send ({} bytes)".format(
                    len(self.build())))

    def build(self):
        if self.req_type == Request.TYPE_MISFIN:
            # Use the MisfinC request format by default
            return self.misfin_c_reqformat()
        else:
            return self.payload

    def misfin_c_reqformat(self):
        # New request format (from the MisfinC proposal)

        # Prepend with 3 lines of (empty) metadata
        # This should be moved to send_as()
        data = '\n\n\n' + self.payload

        return "misfin://{addr}\t{clen}\r\n{payload}".format(
            addr=self.recipient.address(),
            clen=len(data),
            payload=data
        )

    def misfin_b_reqformat(self):
        return "misfin://{} {}\r\n".format(
            self.recipient.address(), self.payload)

    @classmethod
    def incoming(cls, raw: Union[bytes, str]):
        """ Reassemble a Request sent by a client. """

        if isinstance(raw, str):
            # Only for unit tests that pass strings
            raw = raw.encode('utf-8')

        if raw.startswith(b"misfin://"):
            if isinstance(raw, bytes):
                raw = raw.decode("utf-8")

            raw = raw.removeprefix("misfin://")

            # Make sure we have the whole request
            if "\r\n" not in raw:
                raise InvalidRequestError(
                    "Incomplete request - didn't end with crlf")

            # Partition the raw input with CRLF
            _req, _sep, _after = raw.partition("\r\n")

            try:
                assert _sep, "No CRLF found in the request"

                # Check that the request's length (including the CRLF)
                # does not exceed 1024 bytes

                assert len(_req) < (1024 - 2)

                if '\t' in _req:
                    # TAB is in the request: this could be a Misfin(C) request

                    if len(_req.splitlines()) != 1:
                        # TAB was found but CRLF is not on the first line
                        raise InvalidRequestError(
                            "Tab is not in the header"
                        )

                    address, clens = _req.split('\t', 1)

                    clen = int(clens)
                    assert clen > 0

                    # Maximum message length should be inferior to 16384
                    if clen > max_content_length:
                        raise InvalidRequestError(
                            f"Content length value: {clen} exceeds the "
                            f"maximum content length ({max_content_length})"
                        )

                    return cls(Identity(address), _after,
                               content_length=clen)
                else:
                    # Misfin(B) request
                    header, _ = raw.split("\r\n", 1)

                    # Split up the relevant bits of the header
                    address, payload = header.split(" ", 1)
                    return cls(Identity(address), payload,
                               proto_version=Request.MISFIN_PROTO_B)
            except AssertionError as aerr:
                raise InvalidRequestError(str(aerr))
            except (ValueError, TypeError):
                raise
            except BaseException:
                raise InvalidRequestError("Malformed request")
        elif raw.startswith(b"gemini://"):
            # Gemini request

            if isinstance(raw, bytes):
                raw = raw.decode("utf-8")

            if len(raw) > 1024:
                raise ValueError("Gemini request too large")

            urls, _ = raw.split("\r\n", 1)

            return cls(None, raw, req_type=Request.TYPE_GEMINI,
                       req_url=URL(urls.strip()))
        elif raw.startswith(b"titan://"):
            urls, _data = raw.split(b"\r\n", 1)
            url = urls.decode()
            qs = parse_qs(url, separator=';')

            if not qs or 'mime' not in qs or 'size' not in qs:
                raise ValueError('Invalid titan request')

            return cls(None, raw,
                       titan_mime=qs['mime'].pop(),
                       titan_size=int(qs['size'].pop()),
                       titan_data=_data,
                       req_type=Request.TYPE_TITAN,
                       req_url=URL(url.strip()))


class Response:
    """
    Tells the client what to do - either a go ahead, or someflavor of error.
    """

    # Handy error messages for a server to send.
    # Note that 20, 30, and 31 shouldn't use these messages,
    # but they are included here for completeness
    meta_tags = {
        20: "message accepted",

        30: "mailbox changed, look here",
        31: "mailbox changed, look here (permanent)",

        40: "temporary error",
        41: "server is unavailable",
        42: "cgi error",
        43: "proxying error",
        44: "slow down",
        45: "mailbox full",

        50: "permanent error",
        51: "mailbox doesn't exist",
        52: "mailbox has been removed",
        53: "that domain isn't served here",
        59: "bad request",

        60: "certificate required",
        61: "you can't send mail there",
        62: "your certificate is invalid",
        63: "you're lying about your certificate",
        64: "prove it"
    }

    @classmethod
    def of(cls, status: int, meta: Optional[str] = None):
        """ Build a Response object for a status code. """
        ob = cls.__new__(cls)
        ob.status = str(status)
        if meta is None:
            ob.meta = Response.meta_tags[status]
        else:
            ob.meta = meta
        return ob

    # Some shortcuts for responses that actually use the meta tag
    @staticmethod
    def delivered(fingerprint: str):
        return Response.of(20, fingerprint)

    def redirect(to):
        return Response.of(30, to)

    def redirect_forever(to):
        return Response.of(31, to)

    @classmethod
    def incoming(cls, resp):
        """ Creates a Response object from the server's response. """
        ob = cls.__new__(cls)

        # Auto-convert from a bytes object, makes socket code a little cleaner
        if isinstance(resp, bytes):
            resp = resp.decode("utf-8")

        try:
            ob.status, ob.meta = resp.split(" ", 1)
            return ob
        except BaseException:
            raise ValueError("Malformed response")

    def build(self):
        return bytes("{} {}\r\n".format(self.status, self.meta), "utf-8")

    def __str__(self):
        return "{} {}".format(self.status, self.meta)

    def was_successful(self): return self.status[0] == "2"
    def was_redirect(self): return self.status[0] == "3"
    def was_temporary_error(self): return self.status[0] == "4"
    def was_permanent_error(self): return self.status[0] == "5"
    def was_certificate_error(self): return self.status[0] == "6"


def _receive_line(conn, size=max_request_size, timeout=20, until=b"\r\n"):
    """ Receives a Misfin request/response, with configurable timeout etc. """
    """ Note that this doesn't guarantee the received data will be valid... """
    raw = b""
    conn.settimeout(timeout)

    try:
        while len(raw) <= size and until not in raw:
            try:
                raw += conn.recv(size - len(raw))
            except ossl.WantReadError:
                pass

    except socket.timeout:
        pass

    return raw


def _validate_nothing(conn, cert, err, depth, rtrn):
    """ Callback that lets us steal certificate verification from OpenSSL. """
    """
    This is !!!DANGEROUS!!! but necessary to allow us to accept
    self-signed certs.
    """
    return True


def send_as(sender: LocalIdentity, req: Request,
            check_valid_method=_validate_nothing,
            use_ipv6: bool = False):
    """ Sends a Misfin message. """
    # For some reason, this block doesn't survive being moved to a
    # separate function, so it's repeated below in an ugly way.
    context = ossl.Context(ossl.TLS_CLIENT_METHOD)
    context.set_verify(
        ossl.VERIFY_PEER | ossl.VERIFY_FAIL_IF_NO_PEER_CERT,
        callback=check_valid_method)
    context.use_certificate(ocrypt.X509.from_cryptography(sender._cert))
    context.use_privatekey(ocrypt.PKey.from_cryptography_key(sender._private))

    sock = ossl.Connection(
        context,
        socket.socket(
            socket.AF_INET6 if use_ipv6 is True else socket.AF_INET,
            socket.SOCK_STREAM
        )
    )

    try:
        sock.connect((req.recipient.hostname, default_port))
        sock.set_connect_state()
        sock.do_handshake()

        # Send our message and see if the destination accepts.
        sock.sendall(req.build())
        response = Response.incoming(_receive_line(sock))

        # Skadoodle
        sock.shutdown()
        sock.close()
        return response
    except ConnectionRefusedError:
        print(f'Connection to {req.recipient.hostname} '
              f'(port {default_port}) refused')
    except Exception:
        raise


def receive_from(
        conn,
        server: LocalIdentity,
        peer: PeerIdentity,
        on_letter_received,
        server_context,
        addr: Optional[tuple] = None,
        limiter: Optional[Limiter] = None) -> Request:
    """
    Receives a Misfin message from a client
    """

    # Do we want to receive this message?
    try:
        req = Request.incoming(_receive_line(conn))

        if limiter:
            if peer:
                iacquire = peer.fingerprint
            elif addr:
                iacquire = addr[0]
            else:
                iacquire = server.fingerprint

            # Bucket acquire. Misfin requests put more weight on the bucket
            limiter.try_acquire(
                iacquire,
                weight=2 if req.req_type == Request.TYPE_MISFIN else 1
            )

        logger.debug(
            f"Incoming connection from {addr} at "
            f"{datetime.datetime.utcnow()}")

        if req.req_type == Request.TYPE_MISFIN:
            # Misfin request
            if peer is None:
                raise Exception('Certificate required')

            resp = on_letter_received(server_context, server, peer, req)
            conn.sendall(resp.build())
        elif req.req_type == Request.TYPE_GEMINI:
            # Gemini (frontend app) request

            if not server_context.frontend:
                raise Exception('No frontend app')

            try:
                resp = server_context.frontend.get_response(
                    req.req_url, addr, peer._cert if peer else None
                )

                if resp:
                    conn.sendall(bytes(resp))
            except Exception:
                raise
        elif req.req_type == Request.TYPE_TITAN:
            if not server_context.frontend:
                raise Exception('No frontend app')

            try:
                resp = server_context.frontend.get_response(
                    req.req_url, addr,
                    peer._cert if peer else None,
                    titan_mime=req.titan_mime,
                    titan_size=req.titan_size,
                    titan_data=req.titan_data
                )

                if resp:
                    conn.sendall(bytes(resp))
            except Exception:
                raise
    except ossl.ZeroReturnError:
        # The client closed the connection intentionally. Carry on...
        pass
    except ossl.SysCallError:
        # Pretty sure this is also OK...
        pass
    except BucketFullException as bferr:
        # Bucket full

        logger.warning(f'Bucket full for address: {addr}: {bferr}')
        if peer:
            logger.warning(
                f"Bucket full: peer's fingerprint is: {peer.fingerprint}, "
                f"long-form address: {peer.longform()}"
            )

        # Notify the client that he's gotta calm down
        if req.req_type == Request.TYPE_MISFIN:
            # 44: Slow down
            conn.sendall(Response.of(
                44,
                meta='Slow down, rate limit reached (bucket is full)').build()
            )
        elif req.req_type == Request.TYPE_GEMINI:
            conn.sendall('40 Rate limit reached: retry later\r\n'.encode())
    except Exception as err:
        # Something fucked up, be nice and tell the client before handling it.
        traceback.print_exc()

        # Send 59: bad request
        conn.sendall(Response.of(59).build())

        conn.shutdown()
        conn.close()

        raise err

    # Skadoodle
    conn.shutdown()
    conn.close()

    return req


def receive_forever(
        server_context,
        stop_event,
        server: LocalIdentity,
        on_letter_received,
        service_config={},
        check_valid_method=_validate_nothing,
        use_ipv6: bool = False,
        socket_backlog: int = 10,
        listen_addr=None):
    """ Receives Misfin messages, forever and ever. """

    context = ossl.Context(ossl.TLS_SERVER_METHOD)
    context.set_verify(
        ossl.VERIFY_PEER,
        callback=check_valid_method
    )
    context.use_certificate(ocrypt.X509.from_cryptography(server._cert))
    context.use_privatekey(ocrypt.PKey.from_cryptography_key(server._private))
    context.set_session_id(secrets.token_bytes())

    if isinstance(listen_addr, tuple):
        ip6match = re.match(r'^\[([0-9a-fA-F:]+)\]', listen_addr[0])

        if ip6match:
            # IPv6 address enclosed in square brackets

            listen_addr = ip6match.group(1), listen_addr[1]

    if listen_addr:
        try:
            use_ipv6 = isinstance(
                ipaddress.ip_address(listen_addr[0]),
                ipaddress.IPv6Address
            )
        except ValueError:
            pass

    sock_obj = socket.socket(
        socket.AF_INET6 if use_ipv6 is True else socket.AF_INET,
        socket.SOCK_STREAM
    )
    sock_obj.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock_obj.settimeout(15)

    sock = ossl.Connection(context, sock_obj)

    if isinstance(listen_addr, tuple):
        sock.bind(listen_addr)
    else:
        sock.bind((server.hostname, default_port))

    sock.listen(socket_backlog)

    # Rate-limiting config
    rl_cfg = service_config.get('ratelimit', {})
    rl_cfg.setdefault('verified', {
        'req_limit': 100,
        'duration': 60
    })
    rl_cfg.setdefault('anonymous', {
        'req_limit': 30,
        'duration': 60
    })

    anon_limiter = Limiter(
        Rate(rl_cfg['anonymous']['req_limit'],
             Duration.SECOND * rl_cfg['anonymous']['duration'])
    )

    peer_limiter = Limiter(
        Rate(rl_cfg['verified']['req_limit'],
             Duration.SECOND * rl_cfg['verified']['duration'])
    )

    while not stop_event.is_set():
        try:
            # Set up a connection...
            conn, addr = sock.accept()
            conn.set_accept_state()
            conn.do_handshake()

            try:
                peer = PeerIdentity(conn.get_peer_certificate())
            except Exception:
                peer = None

            server_context.threadpool.submit(
                receive_from,
                conn, server, peer, on_letter_received,
                server_context,
                limiter=peer_limiter if peer else anon_limiter,
                addr=addr
            )
        except (ossl.SysCallError, ossl.ZeroReturnError):
            continue
        except socket.timeout:
            continue
        except CancelledError:
            break
        except Exception:
            traceback.print_exc()
