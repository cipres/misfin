from datetime import datetime
import email.errors
import io
import mailbox
import os
import re
import tempfile
import traceback

from typing import Union
from typing import Optional
from pathlib import Path
from abc import ABC
from email.charset import Charset

from dateutil.parser import parse as dt_parse
from dateutil.parser import ParserError

from .crypto import PublicKey, PrivateKey, SealedBox
from .crypto import CryptoError
from .crypto import seal

from .identity import Identity, LocalIdentity
from .misfin import Request


# misfin - development misfin client/server
# misfin letter format ("gemmail")
# lem (2023)


def datetime_z_format(dt: datetime) -> str:
    return dt.strftime("%Y-%m-%dT%H:%M:%SZ")


class Letter:
    """
    A Misfin letter, with the ability to parse gemmail lines
    This class is still being worked on, but is a lot less nasty than it was
    """

    def __init__(
            self,
            sender: LocalIdentity,
            recipients: list[Identity],
            message: str,
            subject: Optional[str] = None,
            received_at: Optional[datetime] = None,
            senders: list[Identity] = [],
            timestamps: list[str] = [],
            format_version: int = Request.MISFIN_PROTO_B,
            encrypted_message: bool = False):
        self.sender = sender
        self.recipients = recipients
        self.subject = subject
        self.message = message
        self.received_at = received_at if received_at else datetime.now()
        self.format_version = format_version

        # Was this message encrypted at rest ?
        self.from_encrypted = encrypted_message

        # Misfin(C) metadata
        self.meta_senders = senders
        self.meta_recipients = recipients
        self.meta_timestamps = timestamps

    def recv_date_format(self, fmt: str) -> str:
        """
        Format the received date with strftime using the given format string
        """
        return self.received_at.strftime(fmt)

    def recv_date_human(self) -> str:
        return self.recv_date_format('%d/%m/%Y %H:%M:%S')

    def _seek_and_destroy(message, linetype):
        """ Extracts out the first line of a given type from the message. """
        found = None
        message_lines = message.splitlines()
        for idx, line in enumerate(message_lines):
            if len(line) > 0 and line[0] == linetype:
                found = line.removeprefix(linetype).strip()
                del message_lines[idx]
                break

        return found, "\n".join(message_lines)

    @staticmethod
    def _extract_sender(message):
        found, message = Letter._seek_and_destroy(message, "<")
        if found is None:
            return None, message

        sender = None
        contents = found.split(" ", 1)
        address = contents[0]
        blurb = contents[1] if len(contents) > 1 else ""

        try:
            sender = Identity(address, blurb=blurb)
        except BaseException:
            pass

        return sender, message

    @staticmethod
    def _parse_identity(data: str) -> Union[Identity, None]:
        parts = data.split(" ", 1)

        if len(parts) > 0:
            address = parts[0]
            blurb = parts[1] if len(parts) > 1 else None

            return Identity(address, blurb=blurb)

        return None

    @staticmethod
    def _extract_recipients(message):
        found, message = Letter._seek_and_destroy(message, ":")
        if found is None:
            return [], message

        recipients = []
        for address in found.split(' '):
            try:
                recipients.append(Identity(address))
            except BaseException:
                pass

        return recipients, message

    @staticmethod
    def _extract_timestamp(message):
        found, message = Letter._seek_and_destroy(message, "@")
        if found is None:
            return None, message

        try:
            timestamp = dt_parse(found)
        except BaseException:
            timestamp = None

        return timestamp, message

    def append_timestamp(self, date=None) -> None:
        """
        Append a timestamp in the timestamps list metadata
        """
        dt = date if date else datetime.now()
        self.meta_timestamps.append(dt)

    @staticmethod
    def _extract_subject(message):
        return Letter._seek_and_destroy(message, "#")

    @classmethod
    def incoming(cls, sender: Identity, req: Request):
        """ Reassembles a Letter from an incoming request. """

        senders: list = []
        recipients: list = []
        timestamps: list = []

        if req.proto_ver == Request.MISFIN_PROTO_C:
            # Misfin(C). Parse the 3 lines of metadata

            stream = io.StringIO(req.payload)

            for x in range(0, 3):
                line = stream.readline().strip()

                if len(line) > 1024:
                    # Metadata lines should not exceed 1024 bytes
                    raise ValueError('Metadata line is longer than 1024 bytes')

                if x == 0 and line:
                    # Senders
                    for s in re.sub(r'^[\s<]*', '', line).split(','):
                        sident = Letter._parse_identity(s.lstrip())
                        if sident:
                            senders.append(sident)
                elif x == 1 and line:
                    # Receivers
                    for recp in re.sub(r'^[\s:]*', '', line).split(','):
                        rident = Letter._parse_identity(recp.lstrip())
                        if rident:
                            recipients.append(rident)
                elif x == 2 and line:
                    # Timestamps
                    for tss in re.sub(r'^[\s:]*', '', line).split(','):
                        try:
                            ts = dt_parse(tss.strip())
                        except ParserError:
                            continue
                        else:
                            timestamps.append(ts)

            if not senders:
                senders.append(sender)

            subject, _ = cls._extract_subject(req.payload)

            return cls(sender,  # type: ignore
                       recipients if recipients else [req.recipient],
                       stream.read(),
                       subject=subject,
                       senders=senders,
                       timestamps=timestamps)
        else:
            frecps, message = cls._extract_recipients(req.payload)
            subject, message = cls._extract_subject(req.payload)

            return cls(sender,  # type: ignore
                       [req.recipient] + frecps if frecps else [req.recipient],
                       message,
                       subject=subject)

    @classmethod
    def load(cls, raw: str, encrypted: bool = False):
        """ Reassembles a Letter from a text file. """

        recipients, raw = cls._extract_recipients(raw)
        sender, raw = cls._extract_sender(raw)
        received_at, raw = cls._extract_timestamp(raw)
        subject, raw = cls._extract_subject(raw)

        return cls(sender, recipients, raw,
                   subject=subject,
                   received_at=received_at,
                   encrypted_message=encrypted)

    def build(self, include_timestamp=True, force_recipients=False) -> str:
        message = "< {} {}\n".format(
            self.sender.address(),
            self.sender.blurb if self.sender.blurb else ''
        )

        if force_recipients or len(self.recipients) > 0:
            message += ": "
            for a in self.recipients:
                message += "{} ".format(a.address())
            message += "\n"

        if include_timestamp and isinstance(self.received_at, datetime):
            message += "@ {}\n".format(datetime_z_format(self.received_at))

        if isinstance(self.subject, str):
            message += f"# {self.subject}\n"

        message += self.message
        return message


class GemBoxSingleFile(ABC):
    mbtype: str = "gembox-file"
    separator = "<====="

    def __init__(self, boxpath: Path):
        self._path: Path = boxpath

        if not self.path.exists():
            self.path.touch()

    @property
    def path(self):
        return self._path.absolute()

    def remove(self, rm_idx: int) -> None:
        msgb = io.StringIO()
        boxb = io.StringIO()
        idx = 0

        with open(self.path, 'rt') as box:
            for line in box:
                msgb.write(line)

                if idx != rm_idx:
                    boxb.write(line)

                if line.rstrip() == self.separator:
                    try:
                        Letter.load(msgb.getvalue())
                    except Exception:
                        continue
                    else:
                        idx += 1
                        msgb.seek(0, 0)
                        msgb.truncate(0)

        with open(self.path, 'wt') as box:
            box.write(boxb.getvalue())

    def __add__(self, message: Letter):
        if not self.path.exists():
            self.path.touch()

        try:
            msgs = message.build()
        except BaseException:
            # Invalid message
            return self

        with open(self._path, 'at') as box:
            box.write(msgs)
            box.write("\n")
            box.write(f"{self.separator}\n")

        return self

    def __getitem__(self, idx):
        try:
            for msgidx, status, msg in self:
                if msgidx == idx:
                    return msg
        except Exception:
            return None

    def __iter__(self):
        """
        Iterator that yields each message in the mailbox
        """
        if not self.path.exists():
            self.path.touch()

        with open(self.path, 'rt') as box:
            msgb = io.StringIO()
            idx = 0

            for line in box:
                if line.rstrip() == self.separator:
                    try:
                        msg = Letter.load(msgb.getvalue())
                        assert msg.sender
                        assert msg.message
                        yield (idx, 0, msg)
                    except Exception:
                        msgb.seek(0, 0)
                        msgb.truncate(0)
                    else:
                        idx += 1

                    msgb.seek(0, 0)
                    msgb.truncate(0)
                else:
                    msgb.write(line)

    def dump(self) -> None:
        with open(self._path, 'rt') as box:
            print(box.read())


class InvalidPrivateKeyError(Exception):
    pass


class GemBoxABC(ABC):
    mbtype: str = "mh"

    MSG_STATUS_NEW = 0
    MSG_STATUS_READ = 1
    MSG_STATUS_TMP = 2
    MSG_STATUS_OTHER = 3
    MSG_STATUS_CANNOT_DECRYPT = 4

    def __init__(self, boxpath: Path, use_locks: bool = False):
        self.__use_locks = use_locks
        self._path: Path = boxpath
        self._box: Union[mailbox.MH, mailbox.Maildir]

        # The private key of the encryption keypair
        self.__privkey: PrivateKey = None

        # The public key of the encryption keypair
        self._pubkey: PublicKey = None

        if self.mbtype == 'mh':
            self._box = mailbox.MH(str(self._path))
        elif self.mbtype == 'maildir':
            self._box = mailbox.Maildir(str(self._path))
        else:
            raise ValueError(f'Invalid mailbox type: {self.mbtype}')

    @property
    def use_locks(self):
        return self.__use_locks

    @property
    def locked(self):
        # Indicates if the mailbox is locked (only meaningful for MH)
        return self._box._locked if self.mbtype == 'mh' else False

    @property
    def enc_pubkey(self) -> PublicKey:
        return self._pubkey

    @property
    def enc_privkey(self) -> PrivateKey:
        return self.__privkey

    @property
    def privkey_is_set(self) -> bool:
        return self.enc_privkey is not None

    @property
    def path(self):
        return self._path.absolute()

    def set_encryption_pubkey(self, pubk: PublicKey) -> None:
        self._pubkey = pubk

    def set_encryption_privkey(self, privk: Union[PrivateKey, None]) -> None:
        self.__privkey = privk

    def __del__(self):
        # Close the box when the object is deleted
        try:
            self._box.close()
        except Exception:
            pass

    def __add__(self, message: Letter):
        if self.use_locks:
            self._box.lock()

        msg = None

        if self.mbtype == 'mh':
            if self.enc_pubkey:
                data = seal(self.enc_pubkey,
                            message.build().encode(encoding='utf-8'))
                msg = mailbox.MHMessage(data)
            else:
                msg = mailbox.MHMessage(message.build())

        elif self.mbtype == 'maildir':
            msg = mailbox.MaildirMessage(message.build())

        # Always use utf-8 charset
        msg.set_charset(Charset('utf-8'))

        self._box.add(msg)
        self._box.flush()

        if self.use_locks:
            self._box.unlock()

        return self

    def remove(self, idx) -> None:
        if self.use_locks:
            self._box.lock()

        self._box.discard(idx)
        self._box.flush()

        if self.use_locks:
            self._box.unlock()

    def mark_read(self, idx) -> bool:
        raise Exception('Not implemented')

    def store_sent(self, message: Letter) -> bool:
        raise Exception('Not implemented')

    def __getitem__(self, idx):
        try:
            return self.__load_message_from_key(idx)
        except Exception:
            return None

    def __load_message_from_key(self, key: Union[int, str]) -> Letter:
        unsealb = SealedBox(self.enc_privkey) if self.enc_privkey else None

        data = self._box[key].get_payload(decode=True)

        try:
            msgu = data.decode('utf-8')
        except UnicodeDecodeError:
            # Check that we have a valid unseal box

            if unsealb is None:
                raise InvalidPrivateKeyError("No priv key set")

            raw = unsealb.decrypt(data)
            return Letter.load(raw.decode(encoding='utf-8'),
                               encrypted=True)
        except CryptoError:
            return None
        else:
            return Letter.load(msgu)

    def __iter__(self):
        for key in self._box.iterkeys():
            try:
                status, message = GemBoxABC.MSG_STATUS_OTHER, self._box[key]
                if self.mbtype == 'maildir':
                    subd = message.get_subdir()
                    if subd == 'new':
                        status = GemBoxABC.MSG_STATUS_NEW
                    elif subd == 'cur':
                        status = GemBoxABC.MSG_STATUS_READ
                    else:
                        status = GemBoxABC.MSG_STATUS_OTHER

                letter = self.__load_message_from_key(key)

                if letter:
                    yield (key, status, letter)
            except email.errors.MessageParseError:
                traceback.print_exc()
                continue
            except InvalidPrivateKeyError:
                # continue
                sender = Identity('invalid@misfin.org')
                recips = [Identity('invalid@misfin.org')]
                yield (key, GemBoxABC.MSG_STATUS_CANNOT_DECRYPT,
                       Letter(sender, recips, ''))
            except BaseException:
                continue

    def dump(self) -> None:
        for root, dirs, files in os.walk(self._path):
            for file in files:
                print(f'{root}/{file}')

    def serialize(self, path: Optional[Path] = None) -> GemBoxSingleFile:
        """
        Serialize to a single-file gembox
        """

        if not path:
            path = Path(tempfile.mkstemp()[1])

        dstbox = GemBoxSingleFile(path)
        for idx, status, msg in self:
            dstbox += msg

        return dstbox


class MHGemBox(GemBoxABC):
    """
    A gembox that uses the MH mailbox format.
    """
    mbtype = 'mh'


class MaildirGemBox(GemBoxABC):
    """
    A gembox that uses the Maildir mailbox format.
    """
    mbtype = 'maildir'

    def mark_read(self, idx) -> bool:
        try:
            if self.use_locks:
                self._box.lock()

            msg = self._box[idx]
            msg.set_subdir('cur')  # type: ignore

            self._box[idx] = msg
            self._box.flush()

            if self.use_locks:
                self._box.unlock()

            return True
        except Exception:
            return False

    def store_sent(self, message: Letter) -> bool:
        sent_folder_name = 'sent'
        try:
            if sent_folder_name not in self._box.list_folders():
                sentf = self._box.add_folder(sent_folder_name)
            else:
                sentf = self._box.get_folder(sent_folder_name)

            sentf.add(mailbox.MaildirMessage(message.build()))
            return True
        except Exception:
            return False


def open_box(mbpath: Path):
    """
    Open a misfin mailbox from the given path by guessing its type
    and return the mailbox object.
    """

    for mbclass in [MHGemBox, MaildirGemBox, GemBoxSingleFile]:
        try:
            mbox = mbclass(mbpath)
            assert list(mbox)
        except Exception:
            continue
        else:
            return mbox


def migrate_gembox(
        src_box: Union[MHGemBox, MaildirGemBox, GemBoxSingleFile],
        dst_box: Union[MHGemBox, MaildirGemBox, GemBoxSingleFile]) -> int:
    """
    Migrate the messages contained in the source gembox to
    the destination gembox. The source gembox will be empty after the
    migration. Returns the number of migrated messages.

    :rtype: int
    """

    mcount: int = 0

    for _id, status, msg in src_box:
        try:
            dst_box += msg

            src_box.remove(_id)
            mcount += 1
        except Exception:
            traceback.print_exc()
            continue

    return mcount
