from typing import Union

from email import message_from_string
from smtpd import SMTPServer

from pyrate_limiter import Duration, Rate, Limiter
from pyrate_limiter import BucketFullException

from . import logger
from .letter import Letter
from .identity import Identity


class SMTPBridgeServer(SMTPServer):
    """
    SMTP <=> Misfin bridge server
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, decode_data=True)

        self.srv_ctx = kwargs.pop('misfin_ctx')
        self.cfg = kwargs.pop('bridge_cfg', {})
        self.srv_ident = self.srv_ctx.server_identity()

        # Rate limiter
        self.msg_limiter = Limiter([
            Rate(self.cfg.get('msg_rate_limit_minute', 3),
                 Duration.MINUTE),
            Rate(self.cfg.get('msg_rate_limit_halfhour', 6),
                 Duration.MINUTE * 30),
            Rate(self.cfg.get('msg_rate_limit_hour', 12),
                 Duration.HOUR),
            Rate(self.cfg.get('msg_rate_limit_day', 25),
                 Duration.DAY)
        ])

    def process_message(self,
                        peer: tuple,
                        mailfrom: str,
                        rcpttos: list,
                        data: Union[bytes, str],
                        **kwargs):
        """
        Process an incoming mail and deliver it to the gembox
        for each of the recipients
        """

        rcpt_idents: list = []

        logger.info(f'SMTP bridge: received message from: {peer}, '
                    f'From: {mailfrom}, to recipients: {rcpttos}')

        try:
            self.msg_limiter.try_acquire(mailfrom)

            msg = message_from_string(
                data.decode() if isinstance(data, bytes) else data
            )

            # Only handle non-multipart messages for now
            assert not msg.is_multipart()

            body = msg.get_payload()
        except BucketFullException as bferr:
            logger.warning(f'SMTP bridge: bucket full ! {bferr}')

            # Reply 550
            return '550 Requested action not taken: ' + \
                'rate limit reached (try again later)'
        except Exception as err:
            logger.debug(f'SMTP bridge: error parsing message: {err}')
            return None

        for rcpt in rcpttos:
            try:
                mbox, host = rcpt.split('@', 1)

                rcpt_ident = self.srv_ctx.ident_for_address(rcpt)
                if not rcpt_ident:
                    continue

                if self.srv_ident.hostname == host:
                    rcpt_idents.append(rcpt_ident)
            except Exception:
                continue

        # Deliver the message to each recipient that has an identity here
        for ident in rcpt_idents:
            gembox = self.srv_ctx.gembox_for(ident)
            if not gembox:
                continue

            logger.info(f'SMTP bridge: delivering to {ident.address()}')

            payload = 'Misfin SMTP bridge: someone sent you a message\n'
            payload += 'Email sent by\n'
            payload += f'=> mailto: {mailfrom}\n'
            payload += body  # type: ignore

            gembox += Letter(
                Identity(self.srv_ident.address()),  # type: ignore
                [
                    Identity(ident.address())
                ],
                payload,
                subject=msg.get('Subject', None)
            )
