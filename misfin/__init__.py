import click
import logging
import pkg_resources

from importlib import resources
from typing import Union
from .misfin import Response


__version__ = "1.2.4"


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def cmsg(message: Union[str, Response], color: str = 'white') -> None:
    """
    Log a message with a certain color
    """
    click.echo(click.style(str(message), fg=color))


def default_mailbox_config_path():
    return pkg_resources.resource_filename(
        'misfin',
        'mailbox_config.toml'
    )


def default_mailbox_peer_config_path():
    return pkg_resources.resource_filename(
        'misfin',
        'mailbox_peer_config.toml'
    )


def resource_filepath(path: str):
    """
    Returns the path to a file in the misfin package.
    """
    return resources.files(__name__) / path


def split_hostport(host_port: str, default_port: int) -> tuple:
    """
    Split a hostname-or-ip:port string and return it as a tuple
    """
    if not host_port.rsplit(':', 1)[-1].isdigit():
        return (host_port, default_port)

    host, port = host_port.rsplit(':', 1)
    return (host, int(port) if port else default_port)
