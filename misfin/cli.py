# misfin - development misfin client/server
# lem (2023)

from pathlib import Path
from typing import Optional

import asyncore
import click
import datetime
from daemonize import Daemonize
import toml
import json
import logging
import concurrent.futures
import signal
import sys
import threading
import traceback
import os

import mergedeep

from . import cmsg
from . import split_hostport
from . import resource_filepath
from .identity import Identity, LocalIdentity
from .misfin import Request
from .misfin import Response
from .misfin import max_content_length
from .service import serve_multi
from .service import serve_single
from .service import ServerContext
from .smtpb import SMTPBridgeServer

from misfin import frontend
from misfin import logger

import misfin.misfin as tmm


def misfind_default_config() -> dict:
    """
    Returns the config from 'misfind_defaults.toml'
    """
    return toml.load(
        resource_filepath('misfind_defaults.toml')
    )


def get_commit_sha() -> Optional[str]:
    """
    Return the SHA of the git commit that the AppImage was built with
    """
    return os.environ.get('MISFIN_COMMIT_SHA')


@click.group()
def cli():
    pass


@click.command()
@click.option('--force', '-f',
              is_flag=True,
              default=False,
              help='Overwrite existing certificate')
@click.argument('mailbox')
@click.argument('name')
@click.argument('hostname')
@click.argument('output')
def make_cert(mailbox: str,
              name: str,
              hostname: str,
              output: str,
              force=False) -> bool:
    """
    Create a certificate
    """

    ident_path = Path(output)
    ident = LocalIdentity.new(mailbox, name, hostname, is_ca=True)

    if ident_path.is_file() and not force:
        raise ValueError(
            f'Identity file {ident_path} already exists ! '
            f'Use --force to overwrite'
        )

    with open(ident_path, "wb") as dest:
        dest.write(ident.as_pem())

    cmsg(f"Generated cert for {ident.longform()} - saved to {output}", 'cyan')
    return True


@click.command()
@click.argument('parent')
@click.argument('mailbox')
@click.argument('blurb')
@click.argument('output')
def cert_from(parent: str,
              mailbox: str,
              blurb: str,
              output: str) -> LocalIdentity:
    """
    Create and sign a certificate using a parent (CA) certificate
    """

    loaded_pem = open(parent, "rb").read()
    parent_ident = LocalIdentity(loaded_pem, loaded_pem)
    ident = LocalIdentity.child_of(parent_ident, mailbox, blurb)

    with open(output, "wb") as dest:
        dest.write(ident.as_pem())

    cmsg(f"Generated cert for {ident.longform()}, "
         f"child of {parent_ident.longform()} - saved to {output}")

    return ident


@click.command()
@click.argument('sender_cert', type=click.Path(exists=True))
@click.argument('recipients')
@click.argument('message')
@click.option('--subject',
              '-s',
              type=str,
              default=None,
              help='Set the subject of the message')
@click.option('--ipv6', '-6',
              is_flag=True,
              default=False,
              help='Force the use of IPv6')
def send_as(sender_cert: str,
            recipients: str,
            message: str,
            subject: str,
            ipv6: bool = False):
    """
    Send a text message.

    The SENDER_CERT argument must be the filepath of a valid
    misfin certificate.

    The RECIPIENTS argument must be a comma-delimited list of
    misfin recipient addresses (example: joe@domain.org,bob@misfin.org).
    """

    exit_code = 0

    loaded_pem = open(sender_cert, "rb").read()
    ident = LocalIdentity(loaded_pem, loaded_pem)

    if subject is not None:
        # Set an explicit subject by adding a heading line
        message = f'# {subject}\n' + message

    for recipient in recipients.split(','):
        msg = Request(Identity(recipient.strip()), message)

        resp = tmm.send_as(ident, msg, use_ipv6=ipv6)
        if isinstance(resp, Response):
            cmsg(resp, color='green' if resp.status == '20' else 'bright_red')
            exit_code = 0 if resp.status == '20' else 1
        else:
            cmsg('No response received from the server', color='bright_red')
            exit_code = 2

    sys.exit(exit_code)


@click.command()
@click.argument('sender_cert', type=click.Path(exists=True))
@click.argument('recipients')
@click.argument('file', type=click.Path(exists=True))
def send_file(sender_cert: str,
              recipients: str,
              file: str):
    """
    Send a text file
    """

    loaded_pem = open(sender_cert, "rb").read()
    ident = LocalIdentity(loaded_pem, loaded_pem)

    textchars = bytearray({7, 8, 9, 10, 12, 13, 27} |
                          set(range(0x20, 0x100)) - {0x7f})

    def is_binary(b): return bool(b.translate(None, textchars))

    fd = open(file, "rb")
    fdata = bytearray()

    while True:
        chunk = fd.read(1024)

        if is_binary(chunk):
            raise ValueError(
                f'{file} seems to be a binary file, '
                'but we can only send text files.'
            )

        fdata.extend(chunk)

        if len(fdata) > max_content_length:
            raise ValueError(f'File is too large: {file}')

        if len(chunk) < 1024:
            break

    for recipient in recipients.split(','):
        msg = Request(Identity(recipient), fdata.decode())

        print(tmm.send_as(ident, msg))

    sys.exit(0)


@click.command()
@click.option('--listen-on',
              '--bind',
              default=None,
              help='Listen on a specific network address: Examples: '
              '"10.0.1.1:1958" or "example.com" or '
              '"[0abc:1def::1234]:1958")')
@click.option('--ipv6', '-6',
              is_flag=True,
              default=False,
              help='Force the use of IPv6')
@click.argument('pem')
def receive_as(pem: str,
               listen_on: str,
               ipv6: bool = False):
    """
    Run a server and receive messages for this identity
    """

    pem_path = Path(pem)
    listen_addr: Optional[tuple] = None
    stop_event = threading.Event()

    logging.basicConfig(level=logging.DEBUG)

    if listen_on:
        listen_addr = split_hostport(listen_on, tmm.default_port)

    if not pem_path.is_file():
        raise ValueError(f'Identity file {pem_path} is not a file')

    loaded_pem = open(pem_path, "rb").read()
    ident = LocalIdentity(loaded_pem, loaded_pem)

    logger.info(f"Receiving for: {ident.longform()}")

    if listen_addr:
        logger.info(f"Listening on: {listen_addr}")
    else:
        logger.info(f"Listening on: {ident.hostname}")

    ctx = ServerContext(Path('.'), pem_path)

    def sig_handler(sig, frame):
        logger.debug(f'Signal {sig} received, bailing out')
        stop_event.set()
        ctx.threadpool.shutdown()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGHUP, sig_handler)

    tmm.receive_forever(ctx,
                        stop_event,
                        ident,
                        serve_single,
                        use_ipv6=ipv6,
                        listen_addr=listen_addr)


default_config = '''
[service.main]
listen_on = "{0}:1958"
cert = "server.pem"

[service.main.ratelimit.anonymous]
req_limit = 30
duration = 60

[service.main.ratelimit.verified]
req_limit = 100
duration = 60
'''


@click.command()
@click.option('--mailbox',
              default='admin',
              help='Set the name of the server mailbox')
@click.argument('srvdir')
@click.argument('hostname')
def server_init(srvdir: str,
                mailbox: str,
                hostname: str) -> bool:
    """
    Initialize a server
    """

    dir_path = Path(srvdir)
    cfg_path = dir_path.joinpath('misfind.toml')
    server_ident_path = dir_path.joinpath('server.pem')

    dir_path.mkdir(parents=True, exist_ok=True)
    dir_path.joinpath('identities').mkdir(parents=True, exist_ok=True)
    dir_path.joinpath('gemboxes').mkdir(parents=True, exist_ok=True)

    if not cfg_path.is_file():
        with open(cfg_path, 'wt') as f:
            f.write(default_config.format(hostname))

    ident = LocalIdentity.new(mailbox, mailbox, hostname, is_ca=True)
    with open(server_ident_path, "wb") as dest:
        dest.write(ident.as_pem())

    return True


@click.command()
@click.option('--verbose', '-v',
              is_flag=True,
              default=False,
              help='Verbose')
@click.option('--force',
              is_flag=True,
              default=False,
              help='Force-start the service (ignores existing PID file)')
@click.option('--daemon',
              '-d',
              is_flag=True,
              default=False,
              help='Run as a daemon')
@click.option('--cname',
              default='main',
              help='Service configuration name')
@click.argument('srvdir')
def serve(srvdir: str, cname: str,
          daemon: bool = False,
          force: bool = False,
          verbose: bool = False):
    """
    Run a full server with multiple identities
    """

    stop_event = threading.Event()

    dir_path = Path(srvdir)
    pid_file = dir_path.joinpath('misfind.pid')
    cfg_path = dir_path.joinpath('misfind.toml')
    old_cfg_path = dir_path.joinpath('misfin.toml')

    if old_cfg_path.is_file() and not cfg_path.is_file():
        old_cfg_path.rename(cfg_path)

    if not dir_path.is_dir():
        raise ValueError(
            f'Service directory {dir_path} does not exist '
            'please use server-init'
        )

    if pid_file.is_file() and not force:
        raise Exception(
            f'PID file {pid_file} exists, is a daemon already running ? '
            'Use --force to force-start the server.'
        )

    if not daemon:
        logging.basicConfig(level=logging.DEBUG)

    try:
        with open(cfg_path, 'rt') as cfgf:
            ucfg = toml.load(cfgf)

        # Merge the defaults with the user config using mergedeep
        cfg = mergedeep.merge({}, misfind_default_config(), ucfg)

        if verbose:
            print(json.dumps(cfg, indent=4))
    except Exception:
        raise

    laddr = None
    srv_cfg = None

    # Find the service config
    for sn, cfg in cfg.get('service', {}).items():
        if sn == cname:
            srv_cfg = cfg

    if not srv_cfg:
        raise ValueError(f'Configuration for {cname} not found')

    server_cert_path = dir_path.joinpath(srv_cfg.get('cert', 'server.pem'))
    server_cert_pem = open(server_cert_path, "rb").read()
    server_ident = LocalIdentity(server_cert_pem, server_cert_pem)

    if 'listen_on' not in srv_cfg:
        laddr = (server_ident.hostname, tmm.default_port)
    else:
        laddr = split_hostport(srv_cfg['listen_on'], tmm.default_port)

    if not laddr:
        raise ValueError('Invalid listen address')

    frontend_cfg = frontend.DefaultConfig()
    frontend_cfg.titan_host = laddr[0] if laddr[0] != '0.0.0.0' else \
        server_ident.hostname
    frontend_cfg.ip = laddr[0]
    frontend_cfg.port = laddr[1]

    ctx = ServerContext(
        Path('.') if daemon else dir_path,
        server_cert_path,
        srv_cfg,
        daemonized=daemon
    )

    # Setup the rate limiters
    ctx.setup_rate_limiters()

    # Gemini frontend app
    ctx.frontend = frontend.misfin_frontend_app(ctx, server_ident,
                                                app_config=frontend_cfg)

    # SMTP bridge
    smtpb_cfg = srv_cfg.get('smtp_bridge', {})

    def sig_handler(sig, frame):
        logger.debug(f'Signal {sig} received, bailing out')

        stop_event.set()

        if ctx.smtpd:
            logger.debug('Shutting down SMTP bridge service')
            ctx.smtpd.close()

        ctx.threadpool.shutdown()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGHUP, sig_handler)

    def run():
        try:
            fh = logging.FileHandler('misfind.log', "w")
            fh.setLevel(logging.DEBUG)
            logger.addHandler(fh)

            if smtpb_cfg and smtpb_cfg.get('enabled', True) is not False:
                if 'listen_on' not in smtpb_cfg:
                    smtp_laddr = (server_ident.hostname, tmm.default_port)
                else:
                    smtp_laddr = split_hostport(smtpb_cfg['listen_on'], 25)

                ctx.smtpd = SMTPBridgeServer(
                    smtp_laddr,
                    None,
                    misfin_ctx=ctx,
                    bridge_cfg=smtpb_cfg
                )

                ctx.threadpool.submit(asyncore.loop, 5)

            commit_sha = get_commit_sha()

            if commit_sha:
                logger.info(f"Running from git commit with SHA: {commit_sha}")

            logger.debug(
                f"Starting misfind at {datetime.datetime.utcnow()}")

            tmm.receive_forever(
                ctx,
                stop_event,
                server_ident,
                serve_multi,
                use_ipv6=srv_cfg.get('use_ipv6', False),
                socket_backlog=srv_cfg.get('socket_backlog', 10),
                listen_addr=laddr,
                service_config=srv_cfg
            )
        except concurrent.futures.CancelledError:
            stop_event.set()
        except OSError as oerr:
            logger.debug(f'OS error: {oerr}')
        except Exception:
            logger.debug(traceback.format_exc())

    if daemon:
        d = Daemonize(
            app="misfin",
            pid=str(pid_file),
            action=run,
            verbose=True,
            chdir=srvdir,
            logger=logger,
            auto_close_fds=False,
            user=srv_cfg.get('user', None),
            group=srv_cfg.get('group', None)

        )
        d.start()
    else:
        run()


@click.command()
@click.argument('gemboxpath')
def gembox_read(gemboxpath):
    """
    Read a gembox
    """

    from misfin import gemboxreader

    return gemboxreader.start_reader(gemboxpath)


@click.command()
@click.option('--sha', '--commit-sha', '-s',
              is_flag=True,
              default=False,
              help='Show the git SHA commit if available')
def version(sha: bool):
    """
    Show the misfin software version or git SHA commit
    """

    from misfin import __version__

    commit_sha = get_commit_sha()

    if commit_sha and sha:
        print(commit_sha)
    else:
        print(__version__)


cli.add_command(make_cert)
cli.add_command(cert_from)
cli.add_command(send_as)
cli.add_command(send_file)
cli.add_command(receive_as)
cli.add_command(serve)
cli.add_command(server_init)
cli.add_command(gembox_read)
cli.add_command(version)


def run():
    return cli()
