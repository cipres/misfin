import base64
from pathlib import Path
from typing import Union

from nacl.public import SealedBox
from nacl.public import PrivateKey
from nacl.public import PublicKey
from nacl.exceptions import CryptoError  # noqa


def generate_keypair():
    key = PrivateKey.generate()

    return key, key.public_key


def privkey_from_base64(b64data: Union[str, bytes]) -> PrivateKey:
    """
    Load a PrivateKey from a base64-encoded string or bytes value
    """
    try:
        return PrivateKey(base64.b64decode(b64data))
    except Exception:
        return None


def privkey_from_base32(b32data: Union[str, bytes]) -> PrivateKey:
    """
    Load a PrivateKey from a base32-encoded string or bytes value
    """
    try:
        return PrivateKey(base64.b32decode(b32data))
    except Exception:
        return None


def privkey_to_base64(privk: PrivateKey) -> str:
    """
    Encode a PrivateKey as a base64-encoded string
    """
    try:
        return base64.urlsafe_b64encode(bytes(privk)).decode('utf-8')
    except Exception:
        return None


def privkey_to_base32(privk: PrivateKey) -> str:
    """
    Encode a PrivateKey as a base32-encoded string
    """
    try:
        return base64.b32encode(bytes(privk)).decode('utf-8')
    except Exception:
        return None


def load_pubkey_from_path(path: Path) -> PublicKey:
    try:
        if path.is_file():
            return PublicKey(path.read_bytes())

        raise ValueError(f'{path} does not exist')
    except Exception:
        return None


def seal(pubk: PublicKey, message):
    sealed_box = SealedBox(pubk)
    return sealed_box.encrypt(message)
