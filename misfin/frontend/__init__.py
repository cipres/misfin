import logging
import os
import os.path
import re
import pkg_resources
import secrets
import subprocess
from datetime import datetime, timedelta
from typing import Optional
from urllib.parse import quote

from yarl import URL

from pyrate_limiter import BucketFullException

from cachetools import TTLCache
from cachetools import cached
from dateutil import parser as du_parser

from gemeaux import App
from gemeaux import Handler
from gemeaux import GemeauxServerConfig

from gemeaux.responses import BadRequestResponse
from gemeaux.responses import TextResponse
from gemeaux.responses import InputResponse
from gemeaux.responses import Response
from gemeaux.responses import DocumentResponse
from gemeaux.responses import RedirectResponse
from gemeaux.responses import Jinja2TemplateResponse

from babel.support import Translations
from jinja2 import Environment
from jinja2 import FileSystemLoader

from tomlkit import aot, nl, table
import tomlkit.exceptions


from ..identity import PeerIdentity
from ..identity import LocalIdentity
from ..identity import Identity
from ..letter import MHGemBox, MaildirGemBox, GemBoxSingleFile
from ..letter import migrate_gembox
from ..letter import InvalidPrivateKeyError
from ..misfin import Request
from ..misfin import send_as

from ..crypto import privkey_to_base32
from ..crypto import privkey_from_base32
from ..crypto import generate_keypair, seal, SealedBox


logger = logging.getLogger(__name__)

supported_langs = ['fr_FR', 'en_US', 'es_ES', 'eo', 'gl']


class DefaultConfig(GemeauxServerConfig):
    certfile = "frontend_cert.pem"
    keyfile = "frontend_key.pem"
    socket_timeout = 5
    titan_host: str


def qget(url: URL) -> str:
    if url.query:
        return list(url.query.keys()).pop(0)
    else:
        return ''


def mailbox_already_exists_input() -> InputResponse:
    return InputResponse(
        'This mailbox already exists. Choose another mailbox name.')


def type_in_recipient_input() -> InputResponse:
    return InputResponse(
        'Type in the destination misfin mail address')


def enter_privkey_input() -> InputResponse:
    return InputResponse(
        'This mailbox contains encrypted messages. Please '
        'enter your private key encoded as base64'
    )


def signup_limits_exceeded() -> TextResponse:
    return TextResponse(
        body='# Server signup limits exceeded\n'
        'Please retry again later !\n'
    )


def root_redirect() -> RedirectResponse:
    return RedirectResponse('/')


class GemBoxDocResponse(DocumentResponse):
    def guess_mimetype(self, filename: str):
        return 'text/gembox'


class RawFileResponse(Response):
    status = 20

    def __init__(self, data: bytes, mimetype: str):
        self.file_data = data
        self.mimetype = mimetype

    def __body__(self):
        return self.file_data


class BaseHandler(Handler):
    def __init__(self, srv_ctx, srv_identity,
                 root_gemini_url: Optional[URL] = None,
                 root_titan_url: Optional[URL] = None):
        super().__init__()

        self.srv_ctx = srv_ctx
        self.srv_identity = srv_identity
        self.root_gemini_url = root_gemini_url
        self.root_titan_url = root_titan_url

    @cached(TTLCache(32, 60 * 30))
    def load_translations(self, lang: str):
        locale_dir = os.environ.get(
            'MISFIN_LOCALE_DIR',
            'locale'
        )

        if os.path.isdir(locale_dir):
            return Translations.load(locale_dir, [lang])

    def get_ts(self, u_ident: LocalIdentity,
               force_lang: Optional[str] = None):
        try:
            assert u_ident is not None
            mcfg = self.srv_ctx.mailbox_config(u_ident)
            lang = mcfg['ui']['lang']
        except BaseException:
            lang = 'en_US'

        if isinstance(force_lang, str) and force_lang in supported_langs:
            lang = force_lang

        return self.load_translations(lang)

    def j2_tmpl_response(self,
                         u_ident: LocalIdentity,
                         tmpl_name: str,
                         **data) -> Jinja2TemplateResponse:
        force_lang = data.pop('_force_lang', None)
        jenv = Environment(
            trim_blocks=True,
            extensions=['jinja2.ext.i18n'],
            loader=FileSystemLoader(pkg_resources.resource_filename(
                'misfin', 'frontend/templates')
            )
        )

        ts = self.get_ts(u_ident, force_lang=force_lang)

        if ts:
            jenv.install_gettext_translations(ts)

        return Jinja2TemplateResponse(
            jenv,
            f'{tmpl_name}.jinja2',
            **data
        )

    @property
    def frontend_service_cfg(self):
        return self.srv_ctx.config.get('frontend')

    @property
    def gemtext_header(self):
        # Template header
        return self.frontend_service_cfg.get('header', '')

    @property
    def gemtext_footer(self):
        # Template footer
        return self.frontend_service_cfg.get('footer', '')

    def get_identity(self, peer_cert) -> tuple:
        """
        Returns, as a tuple of identities, the peer's identity (with no priv
        key), and the matching local identity.
        """
        try:
            identity = PeerIdentity(peer_cert)
            assert self.srv_identity.verify(identity) is True
            return identity, self.srv_ctx.ident_lookup(identity.fingerprint)
        except Exception:
            return None, None


class UserHandler(BaseHandler):
    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        pi, user = self.get_identity(peer_cert)
        if not user:
            return self.j2_tmpl_response(user, 'guest_welcome')

        _ = self.get_ts(user).gettext

        gembox = self.srv_ctx.gembox_for_fingerprint(user.fingerprint)
        if not gembox:
            return TextResponse(body=_('No mailbox found!'))

        query = qget(url)

        cfg = self.srv_ctx.mailbox_config(user)

        try:
            messages = list(gembox)
        except InvalidPrivateKeyError:
            # There are encrypted messages and the private key is
            # not set or is invalid

            return RedirectResponse('/deckey/upload')

        def msg_datesort(entry: tuple):
            try:
                # Convert the message's datetime to a UNIX timestamp
                _id, _status, msg = entry
                return msg.received_at.timestamp()
            except Exception:
                pass

        # Reverse sort the messages list (latest messages will be shown first)
        messages.sort(key=msg_datesort, reverse=True)

        if url.path.startswith('/msg/'):
            if 'msgid' not in route:
                return self.j2_tmpl_response(
                    user,
                    'invalid_input'
                )

            try:
                if gembox.mbtype == 'maildir':
                    idx = route['msgid']
                else:
                    idx = int(route['msgid'])
            except (ValueError, TypeError):
                return self.j2_tmpl_response(
                    user,
                    'invalid_input'
                )

            cmd = route.get('msg_action', None)

            if cmd == 'delete':
                if cfg['mailbox']['deletemsg_confirmation'] is False or \
                        (query and query.lower() == 'yes'):
                    gembox.remove(idx)
                    return root_redirect()
                else:
                    return InputResponse(
                        _('Type in "YES" to confirm the deletion')
                    )
            elif cmd == 'markread':
                if gembox.mbtype == 'maildir':
                    gembox.mark_read(idx)

                return root_redirect()

            if gembox and gembox.privkey_is_set:
                self.srv_ctx.spare_gembox(user.fingerprint)

            msg = gembox[idx]

            if not msg:
                return TextResponse(body=_('Message not found'))

            return self.j2_tmpl_response(
                user,
                'message_show',
                config=cfg,
                message=msg,
                idx=idx
            )
        elif url.path.startswith('/msg_range/') and 0:
            cmd = route.get('msg_action', None)

            if cmd == 'delete':
                ma = re.search(r'^(\d+)\-(\d+)\s*$', query) if query else None
                if not ma:
                    return InputResponse(
                        _('Please type in a message id range')
                    )

                id_first = int(ma.group(1))
                id_last = int(ma.group(2))

                assert id_first >= 0 and id_last > id_first, _("Invalid range")

                for midx in range(id_first, id_last):
                    gembox.remove(midx)

                return root_redirect()
        elif url.path == '/fetch' or url.path == '/mailbox/fetch':
            if cfg['mailbox']['allow_fetch'] is False:
                return BadRequestResponse()

            if type(gembox) in [MHGemBox, MaildirGemBox]:
                fbox = gembox.serialize()
            elif isinstance(gembox, GemBoxSingleFile):
                fbox = gembox
            else:
                raise ValueError('Unknown mailbox type')

            return GemBoxDocResponse(
                str(fbox.path),
                str(fbox.path.parent)
            )
        elif url.path == '/mailbox/clear':
            if not query:
                return InputResponse(
                    _('Type in "YES" to confirm the deletion')
                )
            elif query and query == 'YES':
                gembox._box.clear()
        elif url.path == '/':
            # XXX: If we're using encryption, tell the cache to keep the
            # gembox a bit longer. This means that the user doesn't have to
            # input the PK every time the cache TTL is reached, but only
            # when the user hasn't refreshed the inbox page for a while.

            if gembox and gembox.privkey_is_set:
                self.srv_ctx.spare_gembox(user.fingerprint)

            return self.j2_tmpl_response(
                user,
                'inbox_home',
                config=cfg,
                identity=user,
                gembox=gembox,
                messages=messages,
                header=self.gemtext_header,
                footer=self.gemtext_footer
            )
        elif url.path == '/mailinglists':
            return self.j2_tmpl_response(
                user,
                'ml_index',
                config=cfg,
                identity=user,
                gembox=gembox,
                messages=messages,
                mailing_lists=list(self.srv_ctx.ml_iter()),
                header=self.gemtext_header,
                footer=self.gemtext_footer
            )
        else:
            return self.j2_tmpl_response(
                user,
                'unknown_route',
                identity=user
            )


class FileDownloadHandler(BaseHandler):
    @staticmethod
    def mimetype_from_data(data: bytes) -> str:
        try:
            proc = subprocess.Popen(
                ['file', '--mime-type', '-'],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE
            )
            stdout, stderr = proc.communicate(data)
            spl = stdout.decode().strip().split(':')
            assert len(spl) == 2
        except Exception:
            return 'application/octet-stream'
        else:
            return spl[1].strip()

    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        pi, user = self.get_identity(peer_cert)
        assert user
        _ = self.get_ts(user).gettext

        upload_id = route.get('upload_id')
        query = qget(url)

        if not query:
            return InputResponse(_('Decryption key ?'))

        manifest = self.srv_ctx.get_upload_manifest(upload_id)
        assert manifest, _('Invalid manifest')

        privk = privkey_from_base32(query)
        if not privk:
            return TextResponse(_('Invalid private key'))

        unsealb = SealedBox(privk)

        upload_p = self.srv_ctx.upload_path_for_id(
            manifest['uploader']['fingerprint'],
            manifest['upload_id']
        )

        file_p = upload_p.joinpath(manifest['filename'])
        assert file_p.is_file(), _('File not found')

        with open(file_p, 'rb') as f:
            dec = unsealb.decrypt(f.read())

        assert dec, _('Could not decrypt file')

        mtype = FileDownloadHandler.mimetype_from_data(dec[0:1024])
        assert mtype, _("Could not determine the file's MIME type")

        return RawFileResponse(dec, mtype)


class FileUploadHandler(BaseHandler):
    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        pi, user = self.get_identity(peer_cert)
        assert user
        _ = self.get_ts(user).gettext

        query = qget(url)
        if not query:
            return InputResponse(_('Type in a filename for the upload'))

        return RedirectResponse(
            str(self.root_titan_url.with_path(
                f'/upload_file_encrypted/{query}'
            ))
        )

    def handle_titan(self, url: URL, route, addr: tuple, peer_cert, **kwargs):
        try:
            assert 'filename' in route
            filename = route['filename']
            data = kwargs.pop('titan_data')
            filesize = kwargs.pop('titan_size')
            mime = kwargs.pop('titan_mime')
            assert data

            # Check max filesize
            assert filesize < self.frontend_service_cfg.get(
                'max_fileupload_size',
                1024 * 1024 * 64
            )
        except AssertionError as err:
            return TextResponse(body=f'Invalid titan params: {err}')

        pi, user = self.get_identity(peer_cert)
        assert user, 'Invalid certificate'
        _ = self.get_ts(user).gettext

        user_uploads_p = self.srv_ctx.upload_path_for_identity(user)
        uid = secrets.token_hex(24)

        upload_p = user_uploads_p.joinpath(uid)
        upload_p.mkdir(parents=True, exist_ok=True)

        pubk_p = upload_p.joinpath(f'{filename}.pub')
        file_p = upload_p.joinpath(filename)

        if pubk_p.is_file() or file_p.is_file():
            raise ValueError('Already exists')

        privk, pubk = generate_keypair()
        with open(pubk_p, 'wb') as f:
            f.write(bytes(pubk))

        enc = seal(pubk, data)
        with open(file_p, 'wb') as f:
            f.write(enc)

        self.srv_ctx.write_upload_manifest(user, uid, filename, filesize,
                                           mime)

        privk_b32 = privkey_to_base32(privk)

        return self.j2_tmpl_response(
            user,
            'encrypted_upload_success',
            identity=user,
            upload_id=uid,
            filename=filename,
            base_gemini_url=self.root_gemini_url,
            download_url=self.root_gemini_url.with_path(
                f"/download_enc_file/{uid}"
            ),
            self_download_url=str(self.root_gemini_url.with_path(
                f"/download_enc_file/{uid}"
            )) + f'?{quote(privk_b32)}',
            upload_privkey=privk_b32
        )


class AddressBookHandler(BaseHandler):
    @staticmethod
    def create_ab_entry(addr: str, blurb: Optional[str] = ''):
        entry = aot()
        entry.append(
            table()
            .add('address', addr)
            .add('blurb', blurb)
            .add('date_created', datetime.now().isoformat())
        )
        return entry

    @staticmethod
    def ab_contact_exists(cfg, address: str) -> bool:
        for contact in cfg.get('contacts', []):
            if 'address' in contact and contact['address'] == address:
                return True

        return False

    @staticmethod
    def match_contact(data: str):
        return re.search(
            r'([\w\s]*?)\s*([\w]{1,64}@[\w\.]+)',
            data
        )

    def handle_titan(self, url: URL, route, addr: tuple, peer_cert, **kwargs):
        """
        Handle titan requests
        """

        try:
            assert url.path in ['/addressbook/upload',
                                '/addressbook/import']

            mime = kwargs.pop('titan_mime')
            data = kwargs.pop('titan_data')
            size = kwargs.pop('titan_size')

            assert mime == 'text/plain'
            assert data
            assert size < (1024 * 1024 * 4)
        except AssertionError as err:
            return TextResponse(body=f'Invalid titan params: {err}')
        except Exception:
            return TextResponse(body='Unknown error')

        pi, user = self.get_identity(peer_cert)
        if not user:
            return TextResponse(body='Invalid certificate')

        cfge = self.srv_ctx.mailbox_config(user, 'tomlkit')

        imp_count: int = 0

        for line in data.split('\n'):
            line = line.strip()

            if not line or line.startswith('#'):
                continue

            exists = False
            ma = AddressBookHandler.match_contact(line)

            if not ma:
                continue

            mailbox, _o = ma.group(2).split('@', 1)

            entry = AddressBookHandler.create_ab_entry(
                ma.group(2),
                ma.group(1) if ma.group(1) else mailbox
            )

            exists = AddressBookHandler.ab_contact_exists(cfge, ma.group(2))

            if not exists:
                cfge.add(nl()).add("contacts", entry).add(nl())
                imp_count += 1

        if imp_count > 0:
            self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')
            return TextResponse(body=f'Imported {imp_count} contacts !')
        else:
            return TextResponse(body='Nothing was imported !')

    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        pi, user = self.get_identity(peer_cert)
        if not user:
            return self.j2_tmpl_response(user, 'guest_welcome')

        _ = self.get_ts(user).gettext

        gembox = self.srv_ctx.gembox_for(user)
        if not gembox:
            return TextResponse(body=_('No mailbox found!'))

        query = qget(url)

        cfg = self.srv_ctx.mailbox_config(user)

        if url.path == '/addressbook/list':
            return self.j2_tmpl_response(
                user,
                'addressbook_list',
                identity=user,
                gembox=gembox,
                config=cfg,
                root_titan_url=self.root_titan_url
            )
        elif url.path == '/addressbook/upload_file_encrypted_format':
            return self.j2_tmpl_response(
                user,
                'addressbook_upload_format'
            )
        elif url.path == '/addressbook/delete' and query:
            cfge = self.srv_ctx.mailbox_config(user, 'edit')

            for contact in cfge['contacts']:
                if contact['address'] == query:
                    cfge['contacts'].remove(contact)

                    self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')

                    return TextResponse(
                        body=_('Contact deleted') +
                        '\n=> /addressbook/list Address book\n'
                    )

            return TextResponse(body='Address not found')
        elif url.path == '/addressbook/add':
            if query:
                cfge = self.srv_ctx.mailbox_config(user, 'edit')

                ma = AddressBookHandler.match_contact(query)
                if not ma:
                    return TextResponse(body=_('Invalid contact format'))

                mailbox, _o = ma.group(2).split('@', 1)

                entry = AddressBookHandler.create_ab_entry(
                    ma.group(2),
                    ma.group(1) if ma.group(1) else mailbox
                )

                exists = AddressBookHandler.ab_contact_exists(
                    cfge, ma.group(2))

                if not exists:
                    cfge.add(nl()).add("contacts", entry).add(nl())
                    self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')

                    return TextResponse(
                        body=_('Contact added') +
                        '\n=> /addressbook/list Address book\n')
                else:
                    return TextResponse(body=_('Already exists'))
            else:
                return InputResponse(
                    _('Type in the misfin mail address of the contact '
                      '(e.g "John john@misfin.org" or just "john@misfin.org")')
                )


class SettingsHandler(BaseHandler):
    # Absences cache (caches the input values when registering an absence)
    ab_defs_cache = TTLCache(256, 300)

    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        privk = None
        pi, user = self.get_identity(peer_cert)

        if not user:
            return self.j2_tmpl_response(user, 'guest_welcome')

        _ = self.get_ts(user).gettext

        query = qget(url)
        cfg = self.srv_ctx.mailbox_config(user)
        cfg.setdefault('signature', {
            'sigtext': '',
            'format': 'gemtext',
            'enabled': True
        })
        cfg.setdefault('ui', {'lang': 'en_US'})

        gembox = self.srv_ctx.gembox_for_fingerprint(user.fingerprint)

        setting = route.get('setting')

        if query in ['fetch_disable', 'fetch_enable']:
            cfg['mailbox']['allow_fetch'] = (query == 'fetch_enable')
            self.srv_ctx.mailbox_config_write(user, cfg)
        elif query in ['encryption_enable', 'encryption_disable']:
            enable = (query == 'encryption_enable')

            if enable and not self.srv_ctx.mailbox_has_enckeys(user):
                pubkp, privk = self.srv_ctx.create_enckeys_for_identity(user)

            cfg['storage']['encrypt_incoming_messages'] = enable
            self.srv_ctx.mailbox_config_write(user, cfg)

        if setting == 'encryption_chkey':
            if query != 'YES':
                return InputResponse(
                    _('Type in "YES" to confirm the key change')
                )
            pubkp, privk = self.srv_ctx.create_enckeys_for_identity(
                user, force=True
            )
            return self.j2_tmpl_response(
                user,
                'enckey_changed',
                identity=user,
                config=cfg,
                enc_privkey=privkey_to_base32(privk) if privk else None
            )
        elif setting == 'chmailboxtype' and \
                query in ['mh', 'maildir', 'gembox-file']:
            # Change the storage type and write the config
            cfg['storage']['type'] = query
            self.srv_ctx.mailbox_config_write(user, cfg)

            # Get the new gembox and run the migration
            new_gembox = self.srv_ctx.gembox_for(user)

            if gembox and new_gembox and new_gembox.path != gembox.path:
                # This is a different gembox, migrate messages
                migrate_gembox(gembox, new_gembox)

            return root_redirect()
        elif setting == 'signature_enable':
            cfg['signature']['enabled'] = True
            self.srv_ctx.mailbox_config_write(user, cfg)
        elif setting == 'signature_disable':
            cfg['signature']['enabled'] = False
            self.srv_ctx.mailbox_config_write(user, cfg)
        elif setting == 'chsignature':
            maxslen = 256

            if not query or len(query) > maxslen:
                return InputResponse(
                    _('Please type in your misfin mail signature '
                      f'(gemtext format, max length: {maxslen})')
                )

            cfg['signature']['sigtext'] = query

            self.srv_ctx.mailbox_config_write(user, cfg)

            return RedirectResponse('/settings')
        elif setting == 'chlang' and query in supported_langs:
            cfg['ui']['lang'] = query
            self.srv_ctx.mailbox_config_write(user, cfg)
            return RedirectResponse('/settings')
        elif setting in ['block_addr',
                         'unblock_addr']:
            if not query:
                return InputResponse(_('Type in the misfin address'))

            bk = 'block_senders_byaddr'
            cfge = self.srv_ctx.mailbox_config(user, 'tomlkit')

            if bk not in cfge:
                cfge.add(bk, [])

            if url.path == '/settings/block_addr' and query not in cfge[bk]:
                cfge[bk].append(query)
            elif url.path == '/settings/unblock_addr' and query in cfge[bk]:
                cfge[bk].remove(query)

            self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')
            return TextResponse(
                body=f'Blocked/unblocked: {query}\n=> / Homepage')
        elif setting in ['block_fingerprint',
                         'unblock_fingerprint']:
            if not query:
                return InputResponse('Fingerprint ?')

            fingerprint = query.strip()
            if not re.match(r'[a-z0-9]{64}', fingerprint):
                return TextResponse(body=_('Invalid fingerprint'))

            bk = 'block_senders_byfingerprint'
            cfge = self.srv_ctx.mailbox_config(user, 'tomlkit')

            if bk not in cfge:
                cfge.add(bk, [])

            if url.path == '/settings/block_fingerprint' and \
               fingerprint not in cfge[bk]:
                cfge[bk].append(fingerprint)
            elif url.path == '/settings/unblock_fingerprint' and \
                    fingerprint in cfge[bk]:
                cfge[bk].remove(fingerprint)

            self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')
            return TextResponse(
                body=f'Blocked/unblocked fingerprint: {fingerprint}\n'
                '=> / Homepage')
        elif setting == 'deletemsg_confirmation' and \
                query in ['enable', 'disable']:
            cfg['mailbox']['deletemsg_confirmation'] = (query == 'enable')
            self.srv_ctx.mailbox_config_write(user, cfg)
        elif setting == 'absence_rm' and query:
            # Remove an absence in the config

            cfge = self.srv_ctx.mailbox_config(user, 'tomlkit')
            if 'absence' in cfge:
                cfge['absence'].pop(query)
                self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')
                return TextResponse(_('Removed'))
            else:
                return TextResponse(_('Error'))
        elif setting == 'absence_new':
            # Declare a new absence

            skip = False
            absence = self.ab_defs_cache.get(user.fingerprint, {
                'name': None,
                'start_date': None,
                'end_date': None,
                'autoreply': None
            })

            if not absence['start_date']:
                date = SettingsHandler.parse_date(query) if query else None

                if query and date:
                    absence['start_date'] = date
                    skip = True
                    self.ab_defs_cache[user.fingerprint] = absence
                else:
                    return InputResponse(
                        _('Type in the start date of the absence '
                          '(format: YYYY-MM-DD, example: 2024-09-01)')
                    )
            if not absence['end_date']:
                date = SettingsHandler.parse_date(query) if query else None

                if query and date and not skip:
                    absence['end_date'] = date
                    skip = True
                    self.ab_defs_cache[user.fingerprint] = absence
                else:
                    return InputResponse(
                        _('Type in the end date of the absence '
                          '(format: YYYY-MM-DD)')
                    )
            if not absence['autoreply']:
                if query and not skip:
                    absence['autoreply'] = query
                    skip = True
                    self.ab_defs_cache[user.fingerprint] = absence
                else:
                    return InputResponse(
                        _('Type in the message that will be automatically be '
                          'sent during your absence.')
                    )
            if not absence['name']:
                nma = re.match(r'^\s*([\w]{1,64})', query) if query else None

                if query and not skip and nma:
                    absence['name'] = nma.group(1)
                    self.ab_defs_cache[user.fingerprint] = absence
                else:
                    return InputResponse(
                        _('Type in a display name for the absence')
                    )

            cfge = self.srv_ctx.mailbox_config(user, 'tomlkit')
            try:
                entry = table()
                entry.add('enabled', True)
                entry.add('start_date', absence['start_date'])
                entry.add('end_date', absence['end_date'])
                entry.add('autoreply_message', absence['autoreply'])
                entry.add('autoreply_subject',
                          _('I am currently unavailable'))
                cfge.add('absence', table().add(absence['name'], entry))

                self.srv_ctx.mailbox_config_write(user, cfge, 'tomlkit')
            except tomlkit.exceptions.KeyAlreadyPresent:
                return InputResponse(
                    _('Already exists. Choose another name!')
                )

            if user.fingerprint in self.ab_defs_cache:
                del self.ab_defs_cache[user.fingerprint]

            return TextResponse(_('Success'))

        return self.j2_tmpl_response(
            user,
            'mailbox_settings',
            identity=user,
            config=cfg,
            gembox_privk=privkey_to_base32(privk) if privk else None
        )

    @staticmethod
    def parse_date(text: str) -> Optional[datetime]:
        try:
            return du_parser.parse(text)
        except du_parser.ParserError:
            return None


class MLHandler(BaseHandler):
    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        pi, user = self.get_identity(peer_cert)

        if not user:
            return self.j2_tmpl_response(user, 'guest_welcome')

        _ = self.get_ts(user).gettext

        query = qget(url)
        if url.path == '/mailinglists':
            return self.j2_tmpl_response(
                user,
                'ml_index',
                identity=user,
                mailing_lists=list(self.srv_ctx.ml_iter()),
                header=self.gemtext_header,
                footer=self.gemtext_footer
            )
        elif url.path == '/mailinglists/create':
            if not query:
                return InputResponse(
                    _("Type in the name of the mailing list mailbox "
                      "(followed by an optional blurb between double quotes) "
                      "example: games \"Games mailing list\""
                      ))

            ma = re.search(r'^([\w_\-]{1,32})\s*"?([\w\s]{1,64})?"?\s*$',
                           query)

            if not ma:
                return TextResponse(body=_('Invalid mailbox name format'))

            mailbox_name = ma.group(1)
            mailbox_blurb = ma.group(2)

            if self.srv_ctx.mailbox_exists(mailbox_name):
                return InputResponse(
                    _('This mailbox already exists. '
                      'Choose another mailbox name.')
                )

            ident, pem, privk = self.srv_ctx.create_mailbox(
                self.srv_identity,
                mailbox_name,
                mailbox_blurb if mailbox_blurb else mailbox_name,
                mtype='mailinglist',
                creator=user
            )

            return TextResponse(
                body=_('Mailing list created') +
                '\n=> /mailinglists ' + _('Mailing lists')
            )


class SendHandler(BaseHandler):
    recipients: dict = {}
    drafts_cache = TTLCache(256, 60 * 15)

    def get_draft(self, user):
        entry = self.drafts_cache.get(user.fingerprint)

        if isinstance(entry, tuple):
            return entry

        return (None, None, None)

    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        _i, user = self.get_identity(peer_cert)
        if not user:
            return TextResponse(body='Unauthorized')

        _ = self.get_ts(user).gettext

        cfg = self.srv_ctx.mailbox_config(user)
        query = qget(url)

        if url.path == '/compose':
            if url.query:
                # Store the recipient address
                self.recipients[user.fingerprint] = qget(url)

                return RedirectResponse('/send')
            else:
                return InputResponse(
                    _('Type in the destination misfin mail address')
                )
        elif url.path == '/compose_draft':
            ex_recp, ex_body, ex_dtexp = self.get_draft(user)
            if ex_body:
                return RedirectResponse('/draft')

            self.drafts_cache[user.fingerprint] = (
                query if query else None,  # recipient
                str(),
                datetime.now() + timedelta(seconds=self.drafts_cache.ttl)
            )

            return RedirectResponse('/draft')
        elif url.path == '/draft_rm':
            if query != 'YES':
                return InputResponse(
                    _('Type YES to confirm deletion')
                )

            if user.fingerprint in self.drafts_cache:
                del self.drafts_cache[user.fingerprint]

            return RedirectResponse('/')
        elif url.path == '/draft_edit':
            recp, body, dtexp = self.get_draft(user)

            if query:
                self.drafts_cache[user.fingerprint] = (recp, query, dtexp)

                return RedirectResponse('/draft')
            else:
                return InputResponse(_('Please type in your message'))
        elif url.path == '/draft':
            recp, body, dtexp = self.get_draft(user)

            return self.j2_tmpl_response(
                user,
                'draft_edit',
                draft_id=user.fingerprint,
                identity=user,
                message=body,
                recipient=recp,
                expires=dtexp
            )
        elif url.path == '/draft_send':
            recp, body, dtexp = self.get_draft(user)

            if query and not recp:
                recp = query

            if not recp:
                return InputResponse(
                    _('Type in the destination misfin mail address')
                )

            if not body:
                return RedirectResponse('/draft')

            msg = Request(Identity(recp), body)
            self.srv_ctx.threadpool.submit(send_as, user, msg)

            del self.drafts_cache[user.fingerprint]

            return self.j2_tmpl_response(
                user,
                'message_sent',
                identity=user,
                message=msg
            )
        elif url.path == '/send':
            recipient = self.recipients.get(user.fingerprint)

            if not recipient:
                return RedirectResponse('/compose')
            if not url.query:
                return InputResponse(
                    f'Type in the message to send to: {recipient}'
                )
            elif url.query and recipient:
                body = qget(url)

                sigc = cfg.get('signature')
                if sigc and sigc.get('enabled') is True and 'sigtext' in sigc:
                    body += '\n\n' + sigc['sigtext']

                msg = Request(Identity(recipient), body)

                self.srv_ctx.threadpool.submit(send_as, user, msg)

                self.recipients[user.fingerprint] = None

                return self.j2_tmpl_response(
                    user,
                    'message_sent',
                    identity=user,
                    message=msg
                )
        else:
            # Redirect to compose
            return RedirectResponse('/compose')


class RegisterHandler(BaseHandler):
    """
    Signup handler
    """

    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        user_ip, _port = addr

        lang = route.get('lang', 'en_US')

        _i, user = self.get_identity(peer_cert)
        _ = self.get_ts(user, force_lang=lang).gettext

        reg_message = _(
            'Please type in your desired misfin mailbox name followed by '
            'your name (example: "john John Doe")'
        )

        # Check if signup is enabled in the config
        signup_enabled = self.frontend_service_cfg.get('signup_enable', True)

        if signup_enabled is False:
            # Signup is disabled
            return TextResponse(body=_('Signup is disabled on this server'))

        if user:
            # Already registered, redirect to /
            return root_redirect()

        if not url.query:
            # Ask for a mailbox name
            return InputResponse(reg_message)

        ma = re.search(r'^([\w_]{1,32})\s*"?([\w\s]+)?"?\s*$', qget(url))

        if not ma:
            return InputResponse(
                'Invalid input. ' + reg_message
            )

        mailbox_name, blurb = ma.group(1), ma.group(2)

        if self.srv_ctx.mailbox_exists(mailbox_name):
            return InputResponse(
                _('This mailbox already exists. '
                  'Choose another mailbox name.')
            )

        logger.info(f'Signup request from IP address: {user_ip}')

        # Check that the global signup bucket isn't full
        if self.srv_ctx.signup_limiter_global:
            try:
                self.srv_ctx.signup_limiter_global.try_acquire('signup')
            except BucketFullException as bferr:
                logger.warning(
                    'Global signup rate-limiting bucket full '
                    f'(IP address: {user_ip}): {bferr}'
                )

                return signup_limits_exceeded()

        # Check that the per-user (per IP addr) signup bucket isn't full
        if self.srv_ctx.signup_limiter_user:
            try:
                self.srv_ctx.signup_limiter_user.try_acquire(user_ip)
            except BucketFullException as bferr:
                logger.warning(
                    'Per-user signup rate-limiting bucket full '
                    f'(IP address: {user_ip}): {bferr}'
                )
                return signup_limits_exceeded()

        ident, pem, privk = self.srv_ctx.create_mailbox(
            self.srv_identity,
            mailbox_name,
            blurb if blurb else mailbox_name
        )

        return self.j2_tmpl_response(
            user,
            'mailbox_created',
            _force_lang=lang,
            mailbox=mailbox_name,
            blurb=blurb,
            ident=ident,
            enc_privkey=privkey_to_base32(privk)
        )


class UploadKeyHandler(BaseHandler):
    def get_response(self, url: URL, route, addr: tuple, peer_cert):
        action = route.get('action', 'upload')

        _i, user = self.get_identity(peer_cert)
        assert user
        _ = self.get_ts(user).gettext

        gembox = self.srv_ctx.gembox_for_fingerprint(user.fingerprint)
        assert gembox

        if action == 'unload':
            gembox.set_encryption_privkey(None)
            return root_redirect()

        if not url.query:
            return InputResponse(_(
                'This mailbox contains encrypted messages. Please '
                'enter your private key (base32)'
            ))

        try:
            kspare_ttl = int(route.get('ttl', 0))
        except (TypeError, ValueError):
            kspare_ttl = 0

        privk = privkey_from_base32(qget(url).split('\n').pop(0))
        if not privk:
            return TextResponse(_('Invalid private key'))

        gembox.set_encryption_privkey(privk)

        if kspare_ttl in range(30, 60 * 60):
            self.srv_ctx.spare_gembox(user.fingerprint, kspare_ttl)

        return root_redirect()


def misfin_frontend_app(server_ctx,
                        server_identity: LocalIdentity,
                        app_config=None) -> App:
    main_handler = UserHandler(server_ctx, server_identity)
    send_handler = SendHandler(server_ctx, server_identity)

    # Base Gemini URL
    gemini_url = URL.build(
        scheme='gemini',
        host=server_identity.hostname,
        port=app_config.port
    )

    # Base Titan URL
    titan_url = URL.build(
        scheme='titan',
        host=app_config.titan_host,
        port=app_config.port
    )
    fuh = FileUploadHandler(
        server_ctx,
        server_identity,
        root_gemini_url=gemini_url,
        root_titan_url=titan_url)

    settings_handler = SettingsHandler(server_ctx, server_identity)
    ab_handler = AddressBookHandler(server_ctx, server_identity,
                                    root_titan_url=titan_url)
    ml_handler = MLHandler(server_ctx, server_identity)
    signup_handler = RegisterHandler(server_ctx, server_identity)
    uk_handler = UploadKeyHandler(server_ctx, server_identity)

    urls = {
        "/": main_handler,
        "/msg": main_handler,
        "/msg/{msgid}": main_handler,
        "/msg/{msgid}/{msg_action}": main_handler,
        "/msg_range/{msg_action}": main_handler,
        "/addressbook": ab_handler,
        "/addressbook/{method}": ab_handler,
        "/upload_file_encrypted/{filename}": fuh,
        "/upload": fuh,
        "/download_enc_file/{upload_id}": FileDownloadHandler(
            server_ctx, server_identity
        ),
        "/mailbox/fetch": main_handler,
        "/fetch": main_handler,
        "/settings/{setting}": settings_handler,
        "/settings": settings_handler,
        "/settings/": settings_handler,
        "/compose": send_handler,
        "/compose_draft": send_handler,
        "/draft_rm": send_handler,
        "/draft_edit": send_handler,
        "/draft_send": send_handler,
        "/draft": send_handler,
        "/mailinglists": ml_handler,
        "/mailinglists/create": ml_handler,
        "/send": send_handler,
        "/signup": signup_handler,
        "/signup/{lang}": signup_handler,
        "/deckey/{action}": uk_handler,
        "/deckey/{action}/ttl/{ttl}": uk_handler,
    }

    return App(urls, app_config if app_config else DefaultConfig())
