import sys
import traceback
from pathlib import Path

import click

from . import cmsg
from .letter import open_box


def messages_ls(messages, cur_idx):
    cmsg(f'{len(messages)} messages in your gembox', color='yellow')

    for idx, status, mail in messages:
        recps = ' '.join([recp.address() for recp in mail.recipients])

        cmsg(f'[{idx}]: From: {mail.sender.address()} '
             f'to {recps} ({mail.recv_date_human()})',
             color='yellow' if cur_idx == idx else 'cyan')

    print('\n')


@click.command()
@click.argument('mailbox')
def run(mailbox: str):
    return start_reader(mailbox)


def start_reader(mailbox: str):
    gembox_path = Path(mailbox)

    if not gembox_path.exists():
        print(f'{gembox_path} does not exist')
        sys.exit(1)

    gbox = open_box(gembox_path)

    if not gbox:
        print(f'Could not open mailbox: {gembox_path}')
        sys.exit(2)

    cur_idx = None

    while True:
        messages = list(gbox)

        if not cur_idx:
            messages_ls(messages, cur_idx)

        resp = click.prompt(
            "Enter a message index "
            "('l' to list, 'd' to delete the message, 'q' to quit)")

        if resp == 'q':
            sys.exit(0)
        elif resp == 'l':
            messages_ls(messages, cur_idx)
            continue
        elif resp == 'd':
            if not cur_idx:
                cmsg('Cannot delete (no message selected)', color='bright_red')
                continue

            resp = click.prompt(f'Delete message with index {cur_idx} ? [Y/n]')
            if resp == 'Y':
                gbox.remove(cur_idx)

            continue

        try:
            idx = int(resp)
            msg = gbox[idx]

            if not msg:
                cmsg(f'No message with index {idx}', color='bright_red')
                cur_idx = None
                continue

            cur_idx = idx

            print('\n')
            if msg.subject:
                cmsg(f'Subject: {msg.subject}', color='bright_cyan')

            recps = ' '.join([recp.address() for recp in msg.recipients])

            cmsg(f'Received date: {msg.recv_date_human()}',
                 color='bright_yellow')

            cmsg(f'From: {msg.sender.address()}', color='bright_yellow')
            cmsg(f'To: {recps}\n', color='bright_yellow')
            cmsg(f'{msg.message}\n', color='white')
        except ValueError:
            continue
        except Exception:
            traceback.print_exc()
            continue
