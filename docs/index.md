# Misfin commands

```
Usage: misfin [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  cert-from    Create and sign a certificate using a parent (CA) certificate
  gembox-read  Read a gembox
  make-cert    Create a certificate
  receive-as   Run a server and receive messages for this identity
  send-as      Send a text message
  send-file    Send a file
  serve        Run a full server with multiple identities
  server-init  Initialize a server
  version      Show the misfin software version or git SHA commit
```

# Creating an identity and listening for messages

*Example*: Create an identity for *john@localhost* and receive
messages for this identity:

```sh
misfin make-cert john "John Doe" localhost john.pem
misfin receive-as john.pem
```

*Replace "localhost" by your public domain name or IP*

If you want to listen on a specific address, use **--bind**. Specifying
the port is optional (the default is *1958*). Using **--ipv6** forces the
use of IPv6 for the server socket.

```sh
misfin receive-as --bind '10.0.1.1:1958' john.pem
misfin receive-as --bind '[::1]' john.pem
misfin receive-as --ipv6 --bind 'domain.com' john.pem
```

# Sending a message

*Example*: Create an identity for *alex@localhost* and send the
message "Hello John" to *john@localhost*:

```sh
misfin make-cert alex "Alex" localhost alex.pem
misfin send-as alex.pem john@localhost "Hello John"
```

# Running a server with multiple mailboxes

To initialize a server in the *mydomain* directory for the *mydomain.org* domain
name, use the *server-init* command:

```sh
misfin server-init mydomain mydomain.org
```

You're ready to run the service with:

```sh
misfin serve mydomain
```

Run it as a daemon with **--daemon**, or **-d** (in daemon mode, the log file
and PID file are stored in the service directory):

```sh
misfin serve -d mydomain
```

## Admin interface

Use the [Lagrange](https://github.com/skyjake/lagrange) browser to log in to
the server with the admin identity. First, copy the certificate/key pair to the
clipboard:

```sh
cat mydomain/server.pem | xclip -i -selection clipboard
```

Then, from Lagrange, go to the misfin interface URL, *gemini://mydomain.org:1958*
(replace *mydomain.org* with the hostname you used when initializing the server).
In the *Identity* menu, select *Import*, and press *Ctrl+v* on the keyboard to
paste the certificate and key, then confirm. Now open the identities panel
(*Ctrl+4*), select the certificate you imported, click on it and select *Use on
this page*, then refresh the page. You should now see the admin homepage.

## Configuration

The server config file uses the TOML config file format and is named
**misfind.toml**. In the config file, you can have multiple configuration
classes, the default is *main*. This allows you to try different settings.
To use a different configuration name, use **--cname** when running
**misfin serve**.

### Message delivery logs

*msg_delivery_loglevel* controls the verbosity of the log messages related
to message delivery. Default: 0, value range: 0-5.

```toml
[service.main]
msg_delivery_loglevel = 1
```

### Gembox cache tuning

The *gembox_cache_ttl* and *gembox_cache_size* settings control the
*time-to-live* (in seconds, default: 300) for a box in the gembox TLRU cache,
and the cache size (default: 1024).

```toml
[service.main]
gembox_cache_ttl = 90
gembox_cache_size = 4096
```

### Signup

The *signup* page on the frontend allows users to register a mailbox on your
misfin server.

#### Enabling or disabling

If you don't want users to be able to register an account on the server,
you can disable the *signup* endpoint with the *signup_enable* setting.

```toml
[service.main.frontend]
signup_enable = false
```

If you decide to disable public signups, it is then your responsibility, as
a server admin, to create the mailbox identities yourself by using the
misfin *cert-from* command:

```sh
misfin cert-from srv/server.pem john "John Doe" srv1/identities/john.pem
```

### Virtual mailboxes

You can define virtual mailboxes that can be used for various purposes,
for example an echo service that automatically replies to the sender.

The *payload* variable can be used in the *auto_reply* template
and contains the complete payload of the incoming message.

```toml
[[service.main.vmailbox]]
mailbox = "echo"

# 'mailbox' can also be a list of mailbox names
# mailbox = ["echo", "misfin+echo"]

blurb = "Echo service"
auto_reply = """
This is the echo service. Your message was:

{payload}
"""
```

#### Rate limiting

It is possible to define global and per-user rate limits for the signup endpoint.
The global limits are server scoped, while the user limits apply to individual
IP addresses (the address that the user is signing up from).

In this example, the service allows for 20 signups per minute, as well as
100 signups per hour, but each user is only allowed to create 1 account every
12 hours.

```toml
[service.main.frontend.signup]
global_ratelimits = [ "20/60s", "100/1h" ]
user_ratelimits = [ "1/12h" ]
```

The rate limits must be an array, and each limit must follow the format
*rate/duration*, examples: *20/1s* (20 / second), *10/1m* (10 / minute),
*30/4h* (30 / 4 hours), *1/1d* (1 / day), etc ..

### Header and Footer

*header* and *footer* must be formatted as gemtext.

```toml
[service.main.frontend]
header = """
Welcome!
"""

footer = "Template footer"
```

### Rate limiting

The misfin server limits the number of requests by using the
*Leaky Bucket algorithm* (with
[pyrate-limiter](https://pyratelimiter.readthedocs.io)). There are 2 different
sets of rate-limiting parameters, one for anonymous requests (this would apply
to requests made on the Gemini frontend without a certificate) and one for
requests made with a valid certificate (verified). You can change these
parameters in the config file:

```toml
[service.main.ratelimit.anonymous]
req_limit = 30
duration = 60

[service.main.ratelimit.verified]
req_limit = 100
duration = 60
```

*30 requests / second for anonymous, 100 requests / second for authenticated*

Clients exceeding the bucket limits will receive a code *44* ("slow down") response.

### SMTP bridge

It's possible to run an *SMTP bridge* service, that will deliver incoming
(email) messages to the gembox of the recipient(s) if they have a misfin
identity on the server. To run the *SMTP bridge*, add the following to your
*misfind.toml* config file:

```toml
[service.main.smtp_bridge]
listen_on = "mydomain.org:25"
```

If you're running the SMTP service on a privileged port (like the standard
SMTP port, *25*), you'll need to run the *misfin* server with a privileged
user, you can for example use *sudo* (be sure to add an entry in the *sudoers*
file with the full path of the *misfin* program).

At the moment, the sender of the message is set to be the misfin server
administrator's address, with a notice in the message referencing the original
sender's email address. In a future version, non-misfin email addresses that
send to a misfin address could have a different type of misfin certificate and
it will also be possible to send messages to "regular" email addresses
via the *SMTP bridge*.

The *smtp* to *misfin* message delivery is rate-limited (defaults:
3/minute, 6/half-hour, 12/hour, 25/day).

## Creating mailboxes from the shell

If necessary, you can create mailboxes (in this case *john@mydomain.org* and
*clara@mydomain.org*) from the shell:

```sh
misfin cert-from mydomain/server.pem john "John" mydomain/identities/john.pem
misfin cert-from mydomain/server.pem clara "Clara" mydomain/identities/clara.pem
```

Users can also create their mailbox via the gemini frontend.

## Creating a mailing list

Log into the misfin gemini interface with the admin certificate. On the homepage,
in the *Server administration* section, select *Create mailing list* and enter
the mailbox name of the mailing list (e.g "film" or "linux").

Once created, users can subscribe to the mailing list by sending a message 
containing **# Subscribe** as the payload (a message with the subject
**Subscribe**) to the mailing list address, for example *linux@domain.org*.
The user will receive a verification code that he must reply to
by sending **# Verify CODE** to the mailing list address. To unsubscribe
from the mailing list, send **# Unsubscribe**.

## Mailboxes

By default, the messages will be delivered to a gembox (a mailbox that uses the
MH mailbox format) in the *gemboxes* subdirectory inside the service directory
(the name of the mailbox folder is based on the fingerprint of the recipient's
certificate). The following mailbox types are supported:

- MH (the default)
- Maildir, with the possibility of marking messages as read
- gembox file

## Frontend

There is a frontend gemini application that lets
users create their own mailbox, as well as read and send messages. The frontend
runs on the same address as the misfin server, so if your misfin server runs on
*mydomain.org*, port 1958, the frontend interface can be accessed with a gemini
browser at the following URL: *gemini://mydomain.org:1958*

Here's a list of some of the frontend's gemini endpoints:

- *gemini://mydomain.org:1958/*: Your gembox's homepage. Lists your messages and
  gives access to the address book and settings.
- *gemini://mydomain.org:1958/signup*: Sign up for a mailbox on this server.
- *gemini://mydomain.org:1958/mailbox/fetch*: Fetches your gembox. By default it is
  serialized as a single-file gembox.
- *gemini://mydomain.org:1958/fetch*: Same as */mailbox/fetch*
- *gemini://mydomain.org:1958/settings*: Mailbox config settings page
- *gemini://mydomain.org:1958/compose*: Compose a message. The query string must
  be a valid misfin address.
