misfin (is) mail (for the) small web
====================================

💬 📯 📬

    misfin://hello@misfin.org What's up?\r\n
    ↓↓↓↓
    20 1e:9f:11:e4:8f:aa:12:b3:cb...

What is Misfin ?
----------------

Misfin is to email what Gemini is to the Web. Set up a Misfin server alongside your Gemini capsule, and start networking with other capsuleers - no signup required. For the full details, see gemini://misfin.org/ .

A Misfin message is a single string of UTF-8 gemtext, no more than 2,048 characters long. Want to write more? Send two messages. What about attachments? Host it on a Gemini server and add a link line - you get the fingerprint of your recipient back, so you can even gate access if it's eyes only.
Keeping Misfin mail simple makes it a better fit for the small web - it's easier to implement, easier to secure, and easier to keep running. 

Implementation features
-----------------------

- Server with single or multi-mailbox modes
- Built-in gemini frontend: easy to use interface to read and compose messages
- *i18n*: the gemini frontend interface supports multiple languages (English, French, Castilian Spanish and Esperanto)
- Mailing lists
- Configurable rate-limiting (incoming messages, signup requests)
- Handles Misfin(B) and Misfin(C) requests
- At rest encryption (ECC, *curve25519*) for gembox messages
- Encrypted mail attachments

List of known public misfin servers
===================================

Use a gemini browser to access a public misfin server and register an account.

- [gemini://hashnix.club:1958](gemini://hashnix.club:1958)

Installation
============

AppImage
--------

An AppImage is available, you can
[download it here](https://gitlab.com/cipres/misfin/-/releases/continuous-master/downloads/misfin-latest-x86_64.AppImage). Here is how to install it in
*~/.local/bin*:

```sh
curl -L -o ~/.local/bin/misfin https://gitlab.com/cipres/misfin/-/releases/continuous-master/downloads/misfin-latest-x86_64.AppImage
chmod +x ~/.local/bin/misfin
```

Manual install with pip
-----------------------

You need python version *3.9* or higher. Setup a virtualenv and install with:

```sh
pip install .
```

Documentation
-------------

[Read the misfin documentation](docs/index.md).

Systemd service
---------------

You can find an example of a systemd service file (provided by gritty) [here](systemd/misfin.service).

Community resources
-------------------

- [This repository](https://gitlab.com/cipres/misfin)
- [Issues (GitLab)](https://gitlab.com/cipres/misfin/-/issues)
- [Original implementation by lem](https://sr.ht/~lem/misfin/)

This is a list of gemlogs written by people who are using misfin.

- [Setting up Misfin (written by @gritty from BBS)](gemini://gemini.smallweb.space/tech-gemlog/20240209-misfin.gmi): In this gemlog, gritty describes how to setup
  a misfin server and run it with a systemd service.
- [Misfin: a smallnet messaging protocol (by satch)](gemini://satch.xyz/misfin/)
- [Misfin(C) proposal, draft 9](gemini://gemini.ucant.org/heterodox-tech/proposed-misfin-c-9th-draft.gmi)
