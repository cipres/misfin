import pytest

from misfin.misfin import Request
from misfin.misfin import InvalidRequestError
from misfin.misfin import max_content_length
from misfin.letter import Letter


class TestProtocol:
    def test_requests_b(self, test_identity):
        # Misfin(B) requests

        # No space
        with pytest.raises(ValueError):
            Request.incoming(
                "misfin://me@localhost/"
                "ABCD\r\n"
            )

        # Valid
        req = Request.incoming(
            "misfin://me@localhost "
            "ABCD\r\n"
        )

        assert req.payload == 'ABCD'
        assert req.proto_ver == Request.MISFIN_PROTO_B

        letter = Letter.incoming(test_identity, req)

        assert letter.message == 'ABCD'
        assert letter.sender.address() == 'test@localhost'
        assert letter.recipients[0].address() == 'me@localhost'

        letter.load(letter.build())

    def test_invalid_requests_c(self, test_identity):
        # No CRLF
        with pytest.raises(InvalidRequestError):
            Request.incoming(
                "misfin://me@localhost\t1\n"
                "\n"
                "\n"
                "\n"
                "0"
            )

        # Wrong: the header is incorrect and the TAB is in the metadata
        with pytest.raises(InvalidRequestError):
            Request.incoming(
                "misfin://me@localhost 42\n"
                "\t\n"
                "\r\n"
                "\n"
                "0"
            )

        message = 'Hello'

        # One of the metadata lines is longer than 1024 bytes
        with pytest.raises(ValueError):
            req = Request.incoming(
                f"misfin://me@localhost\t{len(message)}\r\n"
                f"{'AB' * 1024}"
                "\n"
                "\n" + message
            )
            Letter.incoming(test_identity, req)

        # The request is properly formatted but the message's length
        # exceeds the maximum allowed content length (16384)

        large_message = '01' * max_content_length
        assert len(large_message) > max_content_length

        with pytest.raises(InvalidRequestError):
            Request.incoming(
                f"misfin://me@localhost\t{len(large_message)}\r\n"
                "\n"
                "\n"
                "\n" + large_message
            )

    @pytest.mark.parametrize(
        'vmessage', [
            ("This is a test")
        ])
    def test_requests_c(self, vmessage, test_identity):
        # Misfin(C) requests

        req = Request.incoming(
            f"misfin://me@localhost\t{len(vmessage)}\r\n"
            "me@localhost John Dope\n"
            "alex@localhost Alex, sarah@hashnix.club\n"
            "2012-10-09T19:00:55Z\n" + vmessage
        )
        assert req.proto_ver == Request.MISFIN_PROTO_C
        assert req.content_length == len(vmessage)

        letter = Letter.incoming(test_identity, req)
        assert letter.message == vmessage

        assert letter.meta_senders[0].address() == 'me@localhost'
        assert letter.meta_senders[0].blurb == 'John Dope'
        assert letter.recipients[0].address() == 'alex@localhost'
        assert letter.recipients[0].blurb == 'Alex'
        assert letter.recipients[1].address() == 'sarah@hashnix.club'
        assert letter.recipients[1].blurb is None

        assert letter.meta_timestamps[0]

        letter.build()
