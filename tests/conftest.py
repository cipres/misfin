from pathlib import Path
import pytest

from misfin.letter import Letter
from misfin.letter import MHGemBox
from misfin.letter import MaildirGemBox
from misfin.letter import GemBoxSingleFile
from misfin.identity import LocalIdentity
from misfin.identity import Identity


@pytest.fixture
def mh_gembox1(tmpdir):
    return MHGemBox(Path(str(tmpdir.join('gembox1.gbox'))))


@pytest.fixture
def mh_gembox2(tmpdir):
    return MHGemBox(Path(str(tmpdir.join('gembox2.gbox'))))


@pytest.fixture
def maildir_gembox1(tmpdir):
    return MaildirGemBox(Path(str(tmpdir.join('gembox3.maildir'))))


@pytest.fixture
def singlef_gembox1(tmpdir):
    return GemBoxSingleFile(Path(str(tmpdir.join('gembox1.gemboxf'))))


@pytest.fixture
def test_identity():
    return LocalIdentity.new('test', 'test', 'localhost')


@pytest.fixture
def msg1():
    return Letter(
        Identity('john@misfin.org'),
        [
            Identity('ellen@misfin.org'),
            Identity('alex@misfin.org')
        ],
        'This is nice'
    )


@pytest.fixture
def msg2():
    return Letter(
        Identity('ellen@misfin.org'),
        [
            Identity('john@misfin.org'),
            Identity('dave@misfin.org')
        ],
        'I was right'
    )


@pytest.fixture
def msg3():
    return Letter(
        Identity('ellen@misfin.org'),
        [
            Identity('john@misfin.org'),
            Identity('dave@misfin.org')
        ],
        'This',
        subject='Message Subject'
    )
