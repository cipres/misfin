import pytest
from pathlib import Path

from misfin import cli


@pytest.fixture
def srvdir(tmpdir):
    return Path(str(tmpdir.join('misfind')))


@pytest.fixture
def client_cert(tmpdir):
    return Path(str(tmpdir.join('me.pem')))


class TestServer:
    def test_certs(self, srvdir, client_cert):
        assert cli.make_cert.callback('john', 'john', 'localhost',
                                      str(client_cert))
        assert cli.server_init.callback(str(srvdir), 'admin', 'localhost')

        ident = cli.cert_from.callback(
            str(srvdir.joinpath('server.pem')),
            'alex', 'alex',
            str(srvdir.joinpath('identities/alex.pem'))
        )

        assert ident
        assert ident.address() == 'alex@localhost'
