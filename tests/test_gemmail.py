import pytest
from pathlib import Path
from misfin.letter import Letter
from misfin.letter import GemBoxSingleFile
from misfin.letter import MHGemBox
from misfin.letter import MaildirGemBox
from misfin.letter import migrate_gembox
from misfin.identity import Identity
from misfin.misfin import Request

gemm1 = '''
< mailbox@hostname.com me
: one@example.com two@example.com
'''


@pytest.fixture
def msg1():
    return Letter(
        Identity('john@misfin.org'),
        [
            Identity('ellen@misfin.org'),
            Identity('alex@misfin.org')
        ],
        'This is nice'
    )


@pytest.fixture
def msg2():
    return Letter(
        Identity('ellen@misfin.org'),
        [
            Identity('john@misfin.org'),
            Identity('dave@misfin.org')
        ],
        'I was right'
    )


@pytest.fixture
def msg3():
    # Message with utf-8 content

    return Letter(
        Identity('ellen@misfin.org'),
        [
            Identity('john@misfin.org'),
            Identity('dave@misfin.org')
        ],
        '代码错误',
        subject='Зарегистрируйтесь'
    )


@pytest.fixture
def singleboxp(tmpdir):
    return Path(str(tmpdir.join('mymail_single.gbox')))


@pytest.fixture
def boxp(tmpdir):
    return Path(str(tmpdir.join('mymail.gbox')))


@pytest.fixture
def gembox_sample1():
    return Path('tests/test1.gembox')


class TestGemMail:
    """
    Simple parsing and writing tests for the gemmail format
    """

    def test_basic(self):
        l1 = Letter.load(gemm1)
        assert l1
        assert l1.sender.address() == 'mailbox@hostname.com'
        assert l1.sender.blurb == 'me'

        assert l1.recipients[0].address() == 'one@example.com'
        assert l1.recipients[1].address() == 'two@example.com'

    def test_create(self, msg3):
        msg = Letter(
            Identity('john@misfin.org'),
            [
                Identity('ellen@misfin.org'),
                Identity('alex@misfin.org')
            ],
            'This is nice'
        )

        parsed = Letter.load(msg.build())
        assert parsed.recipients[0].address() == 'ellen@misfin.org'
        assert parsed.recipients[1].address() == 'alex@misfin.org'
        assert parsed.message == 'This is nice'

        mwiths = Letter.load(msg3.build())
        assert mwiths.subject == 'Зарегистрируйтесь'
        assert mwiths.message == '代码错误'

    def test_incoming(self):
        data = '\n\n\n'
        data += '# sub\n'
        data += 'test'
        msg = Letter.incoming(
            Identity('john@misfin.org'),
            Request(
                Identity('alex@misfin.org'),
                data
            )
        )
        assert msg.subject == 'sub'


class TestGemBoxSingleFile:
    def test_write(self, singleboxp: Path, msg1, msg2):
        box = GemBoxSingleFile(singleboxp)
        box += msg1
        box += msg2
        box.dump()

        messages = list(box)
        assert len(messages) == 2
        assert messages[0][2].sender.address() == 'john@misfin.org'
        assert len(list(GemBoxSingleFile(singleboxp))) == 2

        box.remove(0)
        assert len(list(box)) == 1

    def test_read(self, gembox_sample1):
        box = GemBoxSingleFile(gembox_sample1)
        assert len(list(box)) == 4

        for idx, status, msg in box:
            assert msg.received_at


class TestGemBox:
    @pytest.mark.parametrize('boxclass', [MHGemBox, MaildirGemBox])
    @pytest.mark.parametrize('mcount', [32, 64, 128])
    def test_nolocks(self, boxp: Path, boxclass, msg1, mcount):
        # Test MH and Maildir gemboxes with no locks
        # Maildir's lock() does nothing but test MaildirGemBox anyway ..

        box = boxclass(boxp, use_locks=False)
        assert box.use_locks is False

        for x in range(0, mcount):
            box += msg1

            if boxclass is MHGemBox:
                assert not box.locked

        assert len(list(box)) == mcount

    def test_write(self, boxp: Path, msg1, msg2):
        box = MHGemBox(boxp)
        box += msg1
        box += msg2
        box.dump()

        messages = list(box)
        assert len(messages) == 2
        assert messages[0][2].sender.address() == 'john@misfin.org'
        assert len(list(MHGemBox(boxp))) == 2

        box.remove(messages[0][0])
        assert len(list(box)) == 1

    def test_maildir_write(self, boxp: Path, msg1, msg2):
        box = MaildirGemBox(boxp)
        box += msg1
        box += msg2
        box.dump()

        messages = list(box)
        assert len(messages) == 2
        assert len(list(MaildirGemBox(boxp))) == 2

        assert box.mark_read(messages[1][0])
        assert box.mark_read(messages[0][0])

    def test_serialize(self, boxp: Path, msg1, msg2, msg3, tmpdir):
        box = MHGemBox(boxp)
        box += msg1
        box += msg2
        box += msg3
        box.dump()

        dst = Path(str(tmpdir.join('serial.gemboxf')))
        boxf = box.serialize(dst)
        assert boxf
        assert len(list(boxf)) == 3

    def test_migrate(self, mh_gembox1, maildir_gembox1, singlef_gembox1,
                     msg1, msg2, msg3):
        for msg in [msg1, msg2, msg3]:
            mh_gembox1 += msg

        assert len(list(mh_gembox1)) == 3

        # MH => Maildir
        assert migrate_gembox(mh_gembox1, maildir_gembox1) == 3
        assert len(list(mh_gembox1)) == 0
        assert len(list(maildir_gembox1)) == 3

        # Maildir => gembox file
        assert migrate_gembox(maildir_gembox1, singlef_gembox1) == 3
        assert len(list(maildir_gembox1)) == 0
        assert len(list(singlef_gembox1)) == 3
