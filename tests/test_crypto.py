import time
import pytest
from pathlib import Path

from freezegun import freeze_time

from misfin.crypto import generate_keypair
from misfin.cache import SparingCache


@pytest.fixture
def keypaths(tmpdir):
    return Path(str(tmpdir.join('priv.key'))), Path(
        str(tmpdir.join('pub.key')))


class TestEncrypt:
    def test_msg_encrypt(self, keypaths, mh_gembox1, msg1, msg2, msg3):
        privp, pubp = keypaths
        privk, pubk = generate_keypair()

        with open(privp, 'wb') as f:
            f.write(bytes(privk))
        with open(pubp, 'wb') as f:
            f.write(bytes(pubk))

        # Set the messages encryption public key for the gembox
        mh_gembox1.set_encryption_privkey(privk)
        mh_gembox1.set_encryption_pubkey(pubk)
        assert mh_gembox1.enc_pubkey is pubk

        # Add a message in the gembox, triggering the encryption
        mh_gembox1 += msg1

        time.sleep(1)

        assert len(list(mh_gembox1)) > 0


class TestKeyCache:
    def test_cache(self, mh_gembox1, msg1):
        key = 'test'
        privk, pubk = generate_keypair()

        with freeze_time() as ft:
            cache = SparingCache(32)
            cache.store(key, privk, 10)
            ft.tick(60)

            with pytest.raises(KeyError):
                item = cache[key]

        privk, pubk = generate_keypair()
        with freeze_time() as ft:
            cache = SparingCache(32)

            cache.store(key, privk, 50)
            ft.tick(40)
            cache.spare(key)
            ft.tick(40)
            assert cache[key]

            cache.spare(key, 150)
            ft.tick(100)
            item = cache[key]
            assert item is privk
